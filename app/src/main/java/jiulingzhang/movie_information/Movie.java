package jiulingzhang.movie_information;

/**
 * Movie model.
 *
 * @author Jiuling Zhang
 * @version 0.0.2
 */
public class Movie {
    private String title;
    private String year;
    private String rating;
    private String runtime;
    private String actors;
    private String plot;
    private String posterUrl;
    private byte[] posterImg;
    private String imdbID;

    public String getTitle() {
        return title;
    }

    public Movie setTitle(String title) {
        this.title = title;
        return this;
    }

    public String getYear() {
        return year;
    }

    public Movie setYear(String year) {
        this.year = year;
        return this;
    }

    public String getRating() {
        return rating;
    }

    public Movie setRating(String rating) {
        this.rating = rating;
        return this;
    }

    public String getRuntime() {
        return runtime;
    }

    public Movie setRuntime(String runtime) {
        this.runtime = runtime;
        return this;
    }

    public String getActors() {
        return actors;
    }

    public Movie setActors(String actors) {
        this.actors = actors;
        return this;
    }

    public String getPlot() {
        return plot;
    }

    public Movie setPlot(String plot) {
        this.plot = plot;
        return this;
    }

    public String getPosterUrl() {
        return posterUrl;
    }

    public Movie setPosterUrl(String posterUrl) {
        this.posterUrl = posterUrl;
        return this;
    }

    public String getImdbID() {
        return imdbID;
    }

    public Movie setImdbID(String imdbID) {
        this.imdbID = imdbID;
        return this;
    }

    public byte[] getPosterImg() {
        return posterImg;
    }

    public Movie setPosterImg(byte[] posterImg) {
        this.posterImg = posterImg;
        return this;
    }
}
