package jiulingzhang.movie_information;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;

public class MovieDatabaseHelper extends SQLiteOpenHelper {
    protected static final String CLASS_NAME = "MovieDatabaseHelper";
    private static MovieDatabaseHelper sInstance;

    public static final String DATABASE_NAME = "movie.db";
    public static final String TABLE_NAME = "movie";
    public static final String KEY_TITLE = "title";
    public static final String KEY_YEAR = "year";
    public static final String KEY_RATING = "rating";
    public static final String KEY_RUNTIME = "runtime";
    public static final String KEY_ACTORS = "actors";
    public static final String KEY_PLOT = "plot";
    public static final String KEY_POSTER_URL = "posterurl";
    public static final String KEY_POSTER_IMG = "posterimg";
    public static final String KEY_IMDBID = "imdbID";
    public static final int VERSION_NUMBER = 3;


    private MovieDatabaseHelper(Context ctx) {
        super(ctx, DATABASE_NAME, null, VERSION_NUMBER);
    }

    public static synchronized MovieDatabaseHelper getInstance(Context context) {
        if (sInstance == null) {
            sInstance = new MovieDatabaseHelper(context.getApplicationContext());
        }
        return sInstance;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        Log.i(CLASS_NAME, "Calling onCreate");
        db.execSQL(
                "create table " + TABLE_NAME
                        + "(" + KEY_IMDBID + " text primary key, "
                        + KEY_TITLE + " text, "
                        + KEY_YEAR + " text, "
                        + KEY_RATING + " text, "
                        + KEY_RUNTIME + " text, "
                        + KEY_ACTORS + " text, "
                        + KEY_PLOT + " text, "
                        + KEY_POSTER_URL + " text, "
                        + KEY_POSTER_IMG + " blob)"
        );
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.i(CLASS_NAME, "Calling onUpgrade, oldVersion=" + oldVersion + " newVersion=" + newVersion);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        onCreate(db);
    }

    public ArrayList<Movie> getMovieRecords() {
        ArrayList movieList = new ArrayList<Movie>();
        SQLiteDatabase db = getWritableDatabase();
        Cursor cursor = db.rawQuery("select * from " + TABLE_NAME, null);
        cursor.moveToFirst();

        while(!cursor.isAfterLast()){
            String title = cursor.getString(cursor.getColumnIndex(MovieDatabaseHelper.KEY_TITLE));
            String year = cursor.getString(cursor.getColumnIndex(MovieDatabaseHelper.KEY_YEAR));
            String rating = cursor.getString(cursor.getColumnIndex(MovieDatabaseHelper.KEY_RATING));
            String runtime = cursor.getString(cursor.getColumnIndex(MovieDatabaseHelper.KEY_RUNTIME));
            String actors = cursor.getString(cursor.getColumnIndex(MovieDatabaseHelper.KEY_ACTORS));
            String plot = cursor.getString(cursor.getColumnIndex(MovieDatabaseHelper.KEY_PLOT));
            String posterurl = cursor.getString(cursor.getColumnIndex(MovieDatabaseHelper.KEY_POSTER_URL));
//            String posterimg = cursor.getString(cursor.getColumnIndex(MovieDatabaseHelper.KEY_POSTER_IMG));
            String imdbID = cursor.getString(cursor.getColumnIndex(MovieDatabaseHelper.KEY_IMDBID));
            Log.i(CLASS_NAME, "SQL ID:" + imdbID);
            Log.i(CLASS_NAME, "SQL Title:" + title);
            Movie movie = new Movie();
            movie.setTitle(title).setYear(year).setRating(rating).setRuntime(runtime).setActors(actors).setPlot(plot).setPosterUrl(posterurl).setImdbID(imdbID);
            movieList.add(movie);
            cursor.moveToNext();
        }
        return movieList;
    }

    public int getIdFromCursor(Cursor cursor){
        SQLiteDatabase db = getWritableDatabase();
        int imdbID = cursor.getInt(cursor.getColumnIndex(KEY_IMDBID));
        return imdbID;
    }

    public long insert(Movie movie){
        Log.i(CLASS_NAME, "Calling insert");
        SQLiteDatabase db = getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(KEY_TITLE, movie.getTitle());
        contentValues.put(KEY_YEAR, movie.getYear());
        contentValues.put(KEY_RATING, movie.getRating());
        contentValues.put(KEY_RUNTIME, movie.getRuntime());
        contentValues.put(KEY_ACTORS, movie.getActors());
        contentValues.put(KEY_PLOT, movie.getPlot());
        contentValues.put(KEY_POSTER_URL, movie.getPosterUrl());
        contentValues.put(KEY_POSTER_IMG, movie.getPosterImg());
        contentValues.put(KEY_IMDBID, movie.getImdbID());
        return db.insert(TABLE_NAME, null, contentValues);
    }

    public boolean delete(String imdbID){
        Log.i(CLASS_NAME, "Calling delete");
        SQLiteDatabase db = getWritableDatabase();
        return db.delete(TABLE_NAME, KEY_IMDBID + "='" + imdbID + "'", null) > 0;
    }


}
