package jiulingzhang.movie_information;

import android.app.Activity;
import android.app.FragmentTransaction;
import android.os.Bundle;
import me.cuiziang.finalproject.R;

/**
 * Activity for displaying a specific movie when in small size device.
 *
 * @author Jiuling Zhang
 * @version 0.0.2
 */
public class MovieDetails extends Activity {
    /**
     * get data from MovieSearchActivity, then pass the data to MovieFragment
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movie_details);

        int position = getIntent().getExtras().getInt("position");

        MovieFragment fragment = new MovieFragment();
        fragment.setMovieWindow(this);

        Bundle bundle = new Bundle();
        bundle.putString("imdbID", getIntent().getExtras().getString("imdbID"));
        bundle.putInt("position", position);
        fragment.setArguments(bundle);

        FragmentTransaction ft = getFragmentManager().beginTransaction();
        ft.replace(R.id.fragment_frame, fragment);
        ft.commit();

    }
}