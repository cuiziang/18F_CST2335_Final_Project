package jiulingzhang.movie_information;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.util.Xml;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import org.xmlpull.v1.XmlPullParser;

import java.io.File;
import java.io.FileOutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import me.cuiziang.finalproject.R;
/**
 * Fragment for displaying a specific movie, will be reused in both small and big size devices.
 *
 * @author Jiuling Zhang
 * @version 0.0.2
 */

public class MovieFragment extends Fragment {
    protected static final String ACTIVITY_NAME = "MovieFragment";

    TextView movieDescriptionView;
    TextView idView;
    Button saveMovieBtn;
    Movie movie;
    Activity movieSearchWindow;

    public MovieFragment() {
        // Required empty public constructor
    }

    /**
     * set MovieSearchActivity
     */
   public void setMovieWindow(Activity movieSearchWindow){
       this.movieSearchWindow = movieSearchWindow;
   }

    public static MovieFragment newInstance()
    {
        MovieFragment myFragment = new MovieFragment();

        return myFragment;
    }

    /**
     * get data from previous activity
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle bundle = this.getArguments();
        if (bundle != null) {
            movie = new Movie();
            movie.setImdbID(bundle.getString("imdbID"));
        }
    }

    /**
     * display movie info and save movie to the local storage.
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_movie_info, container, false);
        movieDescriptionView = (TextView) view.findViewById(R.id.movieDescriptionView);
        new MovieQuery(movie.getImdbID()).execute();


        saveMovieBtn = (Button) view.findViewById(R.id.saveMovieBtn);
        saveMovieBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MovieDatabaseHelper dbHelper = MovieDatabaseHelper.getInstance(movieSearchWindow);
                dbHelper.insert(movie);

                if(movieSearchWindow instanceof MovieSearchActivity){
                    getActivity().getFragmentManager().popBackStack();
                }
                else{
                    Intent intent = new Intent();
                    getActivity().setResult(10, intent);
                    getActivity().finish();
                }
                Toast.makeText(movieSearchWindow.getApplicationContext(), "Movie has been saved", Toast.LENGTH_SHORT).show();

            }
        });
        return view;
    }

    private class MovieQuery extends AsyncTask<String, Integer, String> {

        private String path = "";
        public MovieQuery (String imdbID) {
            this.path = "http://www.omdbapi.com/?i="+imdbID+"&r=xml&apikey=a7644564";
        }
        @Override
        protected String doInBackground(String... strings) {
            HttpURLConnection conn = null;
            try {
                URL url = new URL(path);
                conn = (HttpURLConnection) url.openConnection();
                conn.setReadTimeout(10000);
                conn.setConnectTimeout(15000);
                conn.setRequestMethod("GET");
                conn.setDoInput(true);
                conn.connect();
                int responseCode = conn.getResponseCode();
                if (responseCode == HttpURLConnection.HTTP_OK) {
                    XmlPullParser parser = Xml.newPullParser();
                    parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
                    parser.setInput(conn.getInputStream(), null);
                    parser.nextTag();

                    while (parser.next() != XmlPullParser.END_DOCUMENT) {

                        if (parser.getEventType() != XmlPullParser.START_TAG) {
                            continue;
                        }
                        String name = parser.getName();

                        if (name.equals("movie")) {
                            movie.setTitle(parser.getAttributeValue(null, "title"));
                            movie.setYear(parser.getAttributeValue(null, "year"));
                            movie.setActors(parser.getAttributeValue(null, "actors"));
                            movie.setPlot(parser.getAttributeValue(null, "plot"));
                            movie.setPosterUrl(parser.getAttributeValue(null, "poster"));
                            movie.setRating(parser.getAttributeValue(null, "imdbRating"));
                            movie.setImdbID(parser.getAttributeValue(null, "imdbID"));
                            movie.setRuntime(parser.getAttributeValue(null, "runtime"));
                        }
                        publishProgress(50);
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (conn != null) {
                    conn.disconnect();
                }
            }

            if (fileExistence(movie.getImdbID() + ".png")) {
                Log.i(ACTIVITY_NAME, "Found the image locally!");
            } else {
                Log.i(ACTIVITY_NAME, "Need to download image!");
                try {
                    Bitmap image = Utils.getImage(movie.getPosterUrl());
                    FileOutputStream outputStream = getActivity().openFileOutput(movie.getImdbID() + ".png", Context.MODE_PRIVATE);
                    image.compress(Bitmap.CompressFormat.PNG, 80, outputStream);
                    outputStream.flush();
                    outputStream.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            return null;
        }


        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            String movieDescription = String.format("%s: %s \n %s: %s \n %s: %s \n %s: %s: \n %s: %s \n %s: %s \n %s:%s",
                    getResources().getString(R.string.movie_title),
                    movie.getTitle(),
                    getResources().getString(R.string.movie_year),
                    movie.getYear(),
                    getResources().getString(R.string.movie_rating),
                    movie.getRating(),
                    getResources().getString(R.string.movie_runtime),
                    movie.getRuntime(),
                    getResources().getString(R.string.movie_actors),
                    movie.getActors(),
                    getResources().getString(R.string.movie_plot),
                    movie.getPlot(),
                    getResources().getString(R.string.movie_poster),
                    movie.getPosterUrl());

            movieDescriptionView.setText(movieDescription);
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);
//            progressBar.setProgress(values[0]);
        }
    }

    public boolean fileExistence(String fname) {
        File file = getActivity().getBaseContext().getFileStreamPath(fname);
        return file.exists();
    }

}