package jiulingzhang.movie_information;


import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import java.util.ArrayList;

import me.cuiziang.finalproject.R;
import me.cuiziang.finalproject.ui.FoodListActivity;
import xiachen.ottransport.StationSearchActivity;
import yueli.cbcnewsreader.CBC_News_ReaderActivity;

/**
 * Activity for movie dashboard page.
 *
 * @author Jiuling Zhang
 * @version 0.0.2
 */
public class MovieMainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movie_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.movie_menu, menu);
        return true;
    }

    /**
     * display movie help instruction
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id){
            case R.id.movie_help:
                String title = getResources().getString(R.string.movie_instruction_title);
                String instruction = getResources().getString(R.string.movie_instruction);
                String ok = getResources().getString(R.string.movie_ok);

                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                        MovieMainActivity.this);

                alertDialogBuilder.setTitle(title);
                alertDialogBuilder
                        .setMessage(instruction)
                        .setCancelable(false)
                        .setPositiveButton(ok, null);
                AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();
                return true;
            case R.id.ic_food:
                startActivity(new Intent(MovieMainActivity.this, FoodListActivity.class));
                return true;
            case R.id.ic_news:
                startActivity(new Intent(MovieMainActivity.this, CBC_News_ReaderActivity.class));
                return true;
            case R.id.ic_transportation:
                startActivity(new Intent(MovieMainActivity.this, StationSearchActivity.class));
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    /**
     * navigate to search movie page
     */
    public void navigateToSearchPage(View view) {
        Intent intent = new Intent(MovieMainActivity.this, MovieSearchActivity.class);
        startActivity(intent);
    }

    public void navigateToSavedListPage(View view) {
        Intent intent = new Intent(MovieMainActivity.this, MovieSavedListActivity.class);
        startActivity(intent);
    }

    public void navigateToSummaryPage(View view) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Saved Movie Summary")
                .setMessage(getSummary())
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                    }
                });
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    public String getSummary() {
        ArrayList<Movie> movieList;
        movieList = getMoviesFromLocal();

        int shortest = Integer.MAX_VALUE;
        int longest = Integer.MIN_VALUE;
        int averageTime = 0;
        int averageYear = 0;
        if(movieList.size()>0){
            for (Movie movie: movieList){
                int runTime = 0;
                String runTimeString = movie.getRuntime().split(" ")[0];
                if(runTimeString.matches("\\d+(?:\\.\\d+)?")){
                    runTime = Integer.parseInt(runTimeString);
                }

                int year = 0;
                if(movie.getYear().matches("\\d+(?:\\.\\d+)?")){
                    year = Integer.parseInt(movie.getYear());
                }

                if(runTime < shortest){
                    shortest = runTime;
                }
                if(runTime > longest){
                    longest = runTime;
                }
                averageTime += runTime;
                averageYear += year;
            }
            averageTime /= movieList.size();
            averageYear /= movieList.size();
        }else{
            shortest = 0;
            longest = 0;
        }

        String movieSummary = String.format("%s: %d min\n %s: %d min\n %s: %d min\n %s: %d year",
                getResources().getString(R.string.movie_longest),
                longest,
                getResources().getString(R.string.movie_shortest),
                shortest,
                getResources().getString(R.string.movie_average),
                averageTime,
                getResources().getString(R.string.movie_average_year),
                averageYear);
        return movieSummary;
    }

    public ArrayList<Movie> getMoviesFromLocal() {
        MovieDatabaseHelper dbHelper = MovieDatabaseHelper.getInstance(this);
        return dbHelper.getMovieRecords();
    }
}
