package jiulingzhang.movie_information;

import android.app.Activity;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.FileInputStream;
import java.io.FileNotFoundException;

import me.cuiziang.finalproject.R;

/**
 * Activity for displaying a specific movie when in small size device.
 *
 * @author Jiuling Zhang
 * @version 0.0.2
 */
public class MovieSavedDetails extends Activity {
    private Movie movie;
    private TextView movieDescriptionView;
    private ImageView posterImage;
    int position;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movie_saved_details);

        movieDescriptionView = (TextView) findViewById(R.id.movieDescriptionView);
        posterImage = (ImageView) findViewById(R.id.image_poster);
        movie = new Movie();
        movie.setTitle(getIntent().getExtras().getString("title"));
        movie.setYear(getIntent().getExtras().getString("year"));
        movie.setRating(getIntent().getExtras().getString("rating"));
        movie.setRuntime(getIntent().getExtras().getString("runtime"));
        movie.setActors(getIntent().getExtras().getString("actors"));
        movie.setPlot(getIntent().getExtras().getString("plot"));
        movie.setPosterUrl(getIntent().getExtras().getString("posterurl"));
        movie.setImdbID(getIntent().getExtras().getString("imdbID"));

        String movieDescription = String.format("%s: %s \n %s: %s \n %s: %s \n %s: %s: \n %s: %s \n %s: %s \n %s:%s",
                getResources().getString(R.string.movie_title),
                movie.getTitle(),
                getResources().getString(R.string.movie_year),
                movie.getYear(),
                getResources().getString(R.string.movie_rating),
                movie.getRating(),
                getResources().getString(R.string.movie_runtime),
                movie.getRuntime(),
                getResources().getString(R.string.movie_actors),
                movie.getActors(),
                getResources().getString(R.string.movie_plot),
                movie.getPlot(),
                getResources().getString(R.string.movie_poster),
                movie.getPosterUrl());

        movieDescriptionView.setText(movieDescription);

        FileInputStream fis = null;
        try {
            fis = openFileInput(movie.getImdbID() + ".png");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        Bitmap bitmap = BitmapFactory.decodeStream(fis);
        posterImage.setImageBitmap(bitmap);

        position = getIntent().getExtras().getInt("position");
    }

    public void onDeleteButtonClick(View view) {
        MovieDatabaseHelper dbHelper = MovieDatabaseHelper.getInstance(this);
        dbHelper.delete(movie.getImdbID());
        Toast.makeText(getApplicationContext(), "Movie has been deleted", Toast.LENGTH_SHORT).show();


        Intent intent = new Intent();
        intent.putExtra("position", position);
        setResult(10, intent);
        finish();
    }
}