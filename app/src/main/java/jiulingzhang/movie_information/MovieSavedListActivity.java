package jiulingzhang.movie_information;


import android.app.Activity;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.util.Log;
import android.util.Xml;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.xmlpull.v1.XmlPullParser;

import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

import me.cuiziang.finalproject.R;

/**
 * Activity for displaying saved movies data from local storage.
 *
 * @author Jiuling Zhang
 * @version 0.0.2
 */
public class MovieSavedListActivity extends Activity {
    /**
     * String for debug tag
     */
    protected static final String ACTIVITY_NAME = "MovieSavedListActivity";

    /**
     * for display movie record
     */
    private ListView movieListView;
    /**
     * for storing movie record
     */
    private ArrayList<Movie> movieList;
    /**
     * for adapt local movie records to listview
     */
    private MovieListAdapter movieListAdapter;

    /**
     * Override onCreate Method
     * initialize properties, set listeners, start querying from web server
     *
     * @param savedInstanceState Bundle
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movie_saved_list);


        movieListView = (ListView) findViewById(R.id.movie_list_view);
        movieList = new ArrayList<Movie>();
        movieListAdapter = new MovieListAdapter(this);
        movieListView.setAdapter(movieListAdapter);
        getMoviesFromLocal();

        movieListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Movie selectedMovie = (Movie) parent.getItemAtPosition(position);
            }
        });

        movieListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Movie movie = (Movie) movieList.get(position);

                Intent intent = new Intent(getApplicationContext(), MovieSavedDetails.class);
                intent.putExtra("position", position);
                intent.putExtra("title", movie.getTitle());
                intent.putExtra("year", movie.getYear());
                intent.putExtra("rating", movie.getRating());
                intent.putExtra("runtime", movie.getRuntime());
                intent.putExtra("actors", movie.getActors());
                intent.putExtra("plot", movie.getPlot());
                intent.putExtra("posterurl", movie.getPosterUrl());
                intent.putExtra("imdbID", movie.getImdbID());

                startActivityForResult(intent, 10);
            }
        });


    }

    /**
     * Class for adapting local arraylist to listview
     */
    private class MovieListAdapter extends ArrayAdapter<Movie> {
        public MovieListAdapter(Context ctx) {
            super(ctx, 0);
        }

        public int getCount() {
            return movieList.size();
        }

        public Movie getItem(int position) {
            return movieList.get(position);
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater = MovieSavedListActivity.this.getLayoutInflater();
            View result = inflater.inflate(R.layout.movie_row, null);

            TextView message = (TextView) result.findViewById(R.id.message_text);
            message.setText(getItem(position).getTitle()); // get the string at position
            return result;
        }

        public long getItemId(int position) {
            return position;
        }
    }

    public void getMoviesFromLocal() {
        MovieDatabaseHelper dbHelper = MovieDatabaseHelper.getInstance(this);
        movieList = dbHelper.getMovieRecords();
//        movieListAdapter.notifyDataSetChanged();
    }

    public void onActivityResult(int requestCode, int responseCode, Intent data){
        if(requestCode == 10  && responseCode == 10) {
            Bundle bundle = data.getExtras();
            int position = bundle.getInt("position");
            movieList.remove(position);
            movieListAdapter.notifyDataSetChanged();
        }
    }
}
