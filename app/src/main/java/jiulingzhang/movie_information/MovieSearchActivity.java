package jiulingzhang.movie_information;


import android.app.Activity;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.util.Xml;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.support.design.widget.Snackbar;

import org.xmlpull.v1.XmlPullParser;

import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

import me.cuiziang.finalproject.R;

/**
 * Activity for loading movies data from www.omdbapi.com and populate the data.
 *
 * @author Jiuling Zhang
 * @version 0.0.2
 */
public class MovieSearchActivity extends Activity {
    /**
     * String for debug tag
     */
    protected static final String ACTIVITY_NAME = "MovieSearchActivity";

    /**
     * for display movie record
     */
    private ListView movieListView;
    /**
     * for storing movie record
     */
    private ArrayList<Movie> movieList;
    /**
     * for adapt local movie records to listview
     */
    private MovieListAdapter movieListAdapter;
    /**
     * for display progress when loading data
     */
    private ProgressBar progressBar;

    private EditText titleSearchKey;

    private boolean isTab;


    /**
     * Override onCreate Method
     * initialize properties, set listeners, start querying from web server
     *
     * @param savedInstanceState Bundle
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movie_search);

        titleSearchKey = (EditText) findViewById(R.id.movie_title);
        movieListView = (ListView) findViewById(R.id.movie_list_view);
        movieList = new ArrayList<Movie>();
        movieListAdapter = new MovieListAdapter(this);
        movieListView.setAdapter(movieListAdapter);
        movieListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Movie selectedMovie = (Movie) parent.getItemAtPosition(position);
            }
        });

        progressBar = findViewById(R.id.progressBar);

        FrameLayout frameLayout = (FrameLayout)findViewById(R.id.movie_detail);
        if(frameLayout == null){
            Log.i(ACTIVITY_NAME, "frame is not loaded");
            isTab = false;
        } else{
            Log.i(ACTIVITY_NAME, "frame is loaded");
            isTab = true;
        }

        movieListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Movie  movie = (Movie) movieList.get(position);

                if(isTab){
                    MovieFragment fragment = new MovieFragment();
                    fragment.setMovieWindow(MovieSearchActivity.this);
                    Bundle bundle = new Bundle();
                    bundle.putInt("position",position);
                    bundle.putString("imdbID",movie.getImdbID());
                    fragment.setArguments(bundle);

                    FragmentTransaction ft = getFragmentManager().beginTransaction();
                    ft.replace(R.id.movie_detail, fragment);
                    ft.addToBackStack(null);
                    ft.commit();
                } else{
                    Intent intent = new Intent(getApplicationContext(), MovieDetails.class);
                    intent.putExtra("position",position);
                    intent.putExtra("imdbID",movie.getImdbID());
                    startActivityForResult(intent, 10);
                }
            }
        });
    }


    /**
     * Define action for button
     *
     * @param view View
     */
    public void onSearchButtonClick(View view) {
        String title = titleSearchKey.getText().toString();
        movieList.clear();
        progressBar.setVisibility(View.VISIBLE);
        new MoviesQuery(title).execute();

    }

    /**
     * Define action for button
     *
     * @param view View
     */
    public void saveMovieToStorage(View view) {
        Toast.makeText(getApplicationContext(), "I am a toast", Toast.LENGTH_SHORT).show();
    }

    /**
     * Class for querying from remote web server
     *
     */
    private class MoviesQuery extends AsyncTask<String, Integer, String> {

        private String path = "";
        public MoviesQuery(String searchTerm) {
            this.path = "http://www.omdbapi.com/?s="+searchTerm+"&r=xml&apikey=a7644564";
        }
        @Override
        protected String doInBackground(String... strings) {
            HttpURLConnection conn = null;
            try {
                URL url = new URL(path);
                conn = (HttpURLConnection) url.openConnection();
                conn.setReadTimeout(10000);
                conn.setConnectTimeout(15000);
                conn.setRequestMethod("GET");
                conn.setDoInput(true);
                conn.connect();
                int responseCode = conn.getResponseCode();
                if (responseCode == HttpURLConnection.HTTP_OK) {
                    XmlPullParser parser = Xml.newPullParser();
                    parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
                    parser.setInput(conn.getInputStream(), null);
                    parser.nextTag();

                    while (parser.next() != XmlPullParser.END_DOCUMENT) {

                        if (parser.getEventType() != XmlPullParser.START_TAG) {
                            continue;
                        }
                        String name = parser.getName();

                        if (name.equals("result")) {
                            Movie m = new Movie();
                            m.setTitle(parser.getAttributeValue(null, "title"));
                            m.setImdbID(parser.getAttributeValue(null, "imdbID"));
                            movieList.add(m);
                        }
                        publishProgress(50);
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
                RelativeLayout relativeLayout = (RelativeLayout) findViewById(R.id.relativeLayout);
                Snackbar.make(relativeLayout, "Network Error", Snackbar.LENGTH_LONG).show();
            } finally {
                if (conn != null) {
                    conn.disconnect();
                }
                publishProgress(100);
            }

            return null;
        }


        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            movieListAdapter.notifyDataSetChanged();
            progressBar.setVisibility(View.INVISIBLE);
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);
            progressBar.setProgress(values[0]);
        }
    }

    /**
     * Class for adapting local arraylist to listview
     *
     */
    private class MovieListAdapter extends ArrayAdapter<Movie> {
        public MovieListAdapter(Context ctx) {
            super(ctx, 0);
        }

        public int getCount() {
            return movieList.size();
        }

        public Movie getItem(int position) {
            return movieList.get(position);
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater = MovieSearchActivity.this.getLayoutInflater();
            View result = inflater.inflate(R.layout.movie_row, null);

            TextView message = (TextView) result.findViewById(R.id.message_text);
            message.setText(getItem(position).getTitle()); // get the string at position
            return result;
        }

        public long getItemId(int position) {
            return position;
        }

    }

    /**
     * save movie to local storage
     */
    public void saveMovieToStorage(int position, String id){

//        dbHelper.delete(imdbID, db);
//        messageList.remove(position);
//        messageAdapter.notifyDataSetChanged();

    }
}
