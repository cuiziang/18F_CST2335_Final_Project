package me.cuiziang.finalproject.db;

import android.database.Cursor;
import android.database.CursorWrapper;
import me.cuiziang.finalproject.db.entity.FoodEntity;

import java.util.UUID;

/**
 * Cursor Wrapper of food database
 */
public class FoodCursorWrapper extends CursorWrapper {
    /**
     * Creates a cursor wrapper.
     *
     * @param cursor The underlying cursor to wrap.
     */
    public FoodCursorWrapper(Cursor cursor) {
        super(cursor);
    }

    public FoodEntity getFood() {
        UUID uuid = UUID.fromString(getString(getColumnIndex(FoodDatabaseSchema.FoodTable.Cols.UUID)));
        String tag = getString(getColumnIndex(FoodDatabaseSchema.FoodTable.Cols.TAG));
        String name = getString(getColumnIndex(FoodDatabaseSchema.FoodTable.Cols.NAME));
        int calories = getInt(getColumnIndex(FoodDatabaseSchema.FoodTable.Cols.CALORIE));
        float fat = getFloat(getColumnIndex(FoodDatabaseSchema.FoodTable.Cols.FAT));


        FoodEntity food = new FoodEntity(uuid, tag, name, calories, fat);

        return food;
    }
}
