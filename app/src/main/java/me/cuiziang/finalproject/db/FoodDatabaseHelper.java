package me.cuiziang.finalproject.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Food database helper.
 */
public class FoodDatabaseHelper extends SQLiteOpenHelper {

    private static final int VERSION = 1;
    private static final String DATABASE_NAME = "foodDatabase.db";

    public FoodDatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("create table " + FoodDatabaseSchema.FoodTable.NAME + "(" + " _id integer primary key " + "autoincrement, " + FoodDatabaseSchema.FoodTable.Cols.TAG + ", " + FoodDatabaseSchema.FoodTable.Cols.NAME + ", " + FoodDatabaseSchema.FoodTable.Cols.UUID + ", " + FoodDatabaseSchema.FoodTable.Cols.CALORIE + ", " + FoodDatabaseSchema.FoodTable.Cols.FAT + ")");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
