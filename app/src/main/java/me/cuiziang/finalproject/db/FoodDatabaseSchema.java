package me.cuiziang.finalproject.db;

/**
 * Schema of food database
 */
public class FoodDatabaseSchema {
    public static final class FoodTable {
        public static final String NAME = "foods";

        public static final class Cols {
            public static final String UUID = "uuid";
            public static final String TAG = "tag";
            public static final String NAME = "name";
            public static final String CALORIE = "calorie";
            public static final String FAT = "fat";
        }
    }

}
