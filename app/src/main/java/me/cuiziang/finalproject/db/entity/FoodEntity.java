package me.cuiziang.finalproject.db.entity;

import me.cuiziang.finalproject.model.Food;

import java.io.Serializable;
import java.util.UUID;

public class FoodEntity implements Serializable, Food {

    private UUID id;
    private String tag;
    private String name;

    public FoodEntity(String name) {
        this(UUID.randomUUID());
        this.name = name;
    }

    private int calories;
    private float fat;

    public FoodEntity(String tag, String name, int calories, float fat) {
        this.id = UUID.randomUUID();
        this.tag = tag;
        this.name = name;
        this.calories = calories;
        this.fat = fat;
    }

    public FoodEntity(UUID uuid, String tag, String name, int calories, float fat) {
        this.id = uuid;
        this.tag = tag;
        this.name = name;
        this.calories = calories;
        this.fat = fat;
    }

    public FoodEntity(UUID id) {
        this.id = id;
    }

    public UUID getId() {
        return id;
    }

    @Override
    public int getCalories() {
        return calories;
    }

    public void setCalories(int calories) {
        this.calories = calories;
    }

    public void setFat(float fat) {
        this.fat = fat;
    }

    @Override
    public float getFat() {
        return fat;
    }

    @Override
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }
}
