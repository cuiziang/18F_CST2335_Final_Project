package me.cuiziang.finalproject.model;

import java.util.UUID;

/**
 * Model of food
 */
public interface Food {
    UUID getId();

    String getTag();

    String getName();

    int getCalories();

    float getFat();
}
