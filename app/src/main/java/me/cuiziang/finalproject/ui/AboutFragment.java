package me.cuiziang.finalproject.ui;

import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import me.cuiziang.finalproject.R;

import java.util.Objects;

/**
 * Dialog class.
 */
public class AboutFragment extends DialogFragment {

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        String help = getResources().getString(R.string.food_help);
        return new AlertDialog.Builder(Objects.requireNonNull(getActivity())).setTitle("Help").setMessage(help).setPositiveButton(android.R.string.ok, null).create();
    }
}
