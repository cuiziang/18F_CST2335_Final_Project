package me.cuiziang.finalproject.ui;

import android.content.Context;
import android.content.Intent;
import android.support.v4.app.Fragment;

import java.io.Serializable;

/**
 * The activity which host fragment of food detail.
 */
public class FoodDetailActivity extends SingleFragmentActivity {

    public static Intent newIntent(Context packageContext, Serializable food) {
        Intent intent = new Intent(packageContext, FoodDetailActivity.class);
        intent.putExtra(FoodDetailFragment.EXTRA_FOOD, food);
        return intent;
    }

    protected Fragment createFragment() {
        Serializable food = getIntent().getSerializableExtra(FoodDetailFragment.EXTRA_FOOD);
        return FoodDetailFragment.newInstance(food);
    }
}
