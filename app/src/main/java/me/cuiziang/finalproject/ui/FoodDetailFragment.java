package me.cuiziang.finalproject.ui;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.*;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import me.cuiziang.finalproject.R;
import me.cuiziang.finalproject.db.entity.FoodEntity;
import me.cuiziang.finalproject.viewmodel.FoodViewModel;

import javax.net.ssl.HttpsURLConnection;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.net.URL;
import java.util.Objects;

/**
 * Controller of food detail.
 */
public class FoodDetailFragment extends Fragment {

    private static final String BASE_URL = "https://api.edamam.com/api/food-database/parser";
    private static final String APPLICATION_ID = "2615767c";
    private static final String APPLICATION_KEYS = "456dbe3380ff334a7cbaf66d6611aa4a";
    public static String EXTRA_FOOD;
    private FoodViewModel foodViewModel;
    private LinearLayout foodDetailLayout;
    private TextView searchedFoodNameTextView;
    private TextView caloriesTextView;
    private TextView fatTextView;
    private ProgressBar fetchFoodDataProgressBar;

    private FoodEntity food;

    public static FoodDetailFragment newInstance(Serializable food) {
        Bundle args = new Bundle();
        args.putSerializable(EXTRA_FOOD, food);
        FoodDetailFragment fragment = new FoodDetailFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_food_detail, container, false);

        foodViewModel = FoodViewModel.get(getContext());

        searchedFoodNameTextView = view.findViewById(R.id.searchedFoodNameTextView);
        caloriesTextView = view.findViewById(R.id.caloriesTextView);
        fatTextView = view.findViewById(R.id.fatTextView);
        fetchFoodDataProgressBar = view.findViewById(R.id.fetchFoodDataProgressBar);
        foodDetailLayout = view.findViewById(R.id.foodDetailLayout);

        food = foodViewModel.getFood(((FoodEntity) Objects.requireNonNull(Objects.requireNonNull(getArguments()).getSerializable(EXTRA_FOOD))).getId());

        if (food != null) {
            updateUI();
        } else {
            new FoodDataQuery().execute();
        }

        foodDetailLayout.setOnLongClickListener(v -> {
            Vibrator vibrator = (Vibrator) getContext().getSystemService(Context.VIBRATOR_SERVICE);
            if (foodViewModel.getFood(food.getId()) == null) {
                foodViewModel.addFood(food);
                Snackbar.make(foodDetailLayout, "Food Saved", Snackbar.LENGTH_SHORT).show();
                if (Build.VERSION.SDK_INT >= 26) {
                    vibrator.vibrate(VibrationEffect.createOneShot(1000, VibrationEffect.DEFAULT_AMPLITUDE));
                } else {
                    vibrator.vibrate(1000);
                }
            } else {
                foodViewModel.deleteFood(food.getId());
                Snackbar.make(foodDetailLayout, "Food Unsaved", Snackbar.LENGTH_SHORT).show();
                if (Build.VERSION.SDK_INT >= 26) {
                    vibrator.vibrate(VibrationEffect.createOneShot(1000, VibrationEffect.DEFAULT_AMPLITUDE));
                } else {
                    vibrator.vibrate(1000);
                }
            }
            return true;
        });

        return view;
    }

    private void updateUI() {
        fetchFoodDataProgressBar.setVisibility(View.GONE);
        searchedFoodNameTextView.setText(getString(R.string.foodsearching) + "\n" + food.getName());
        caloriesTextView.setText(getString(R.string.food_calories_searching) + "\n" + food.getCalories());
        fatTextView.setText(getString(R.string.food_fat_searching) + "\n" + food.getFat());
    }


    private class FoodDataQuery extends AsyncTask<String, Integer, String> {

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if (food != null) {
                updateUI();
            } else {
                Toast.makeText(getContext(), R.string.food_not_in_database, Toast.LENGTH_SHORT).show();
                Intent intent = FoodListActivity.newIntent(getContext());
                Objects.requireNonNull(getActivity()).setResult(Activity.RESULT_OK, intent);
                getActivity().finish();
            }
        }

        @Override
        protected void onProgressUpdate(Integer... values) {

        }

        @Override
        protected String doInBackground(String... strings) {
            URL url = null;
            StringBuilder json = new StringBuilder();
            try {
                url = new URL(BASE_URL + "?" + "ingr=" + ((FoodEntity) Objects.requireNonNull(Objects.requireNonNull(getArguments()).getSerializable(EXTRA_FOOD))).getName() + "&app_id=" + APPLICATION_ID + "&app_key=" + APPLICATION_KEYS);
                HttpsURLConnection con = (HttpsURLConnection) url.openConnection();
                BufferedReader br = new BufferedReader(new InputStreamReader(con.getInputStream()));
                String inputLine;
                while ((inputLine = br.readLine()) != null) {
                    json.append(inputLine);
                }
                br.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
            JsonObject jsonElement = new JsonParser().parse(Objects.requireNonNull(json).toString()).getAsJsonObject();
            if (jsonElement.get("parsed").getAsJsonArray().size() != 0) {
                String name = jsonElement.get("parsed").getAsJsonArray().get(0).getAsJsonObject().get("food").getAsJsonObject().get("label").getAsString();
                int calories = jsonElement.get("parsed").getAsJsonArray().get(0).getAsJsonObject().get("food").getAsJsonObject().get("nutrients").getAsJsonObject().get("ENERC_KCAL").getAsInt();
                float fat = jsonElement.get("parsed").getAsJsonArray().get(0).getAsJsonObject().get("food").getAsJsonObject().get("nutrients").getAsJsonObject().get("FAT").getAsFloat();
                food = new FoodEntity(name, name, calories, fat);
            }

            return null;
        }
    }
}
