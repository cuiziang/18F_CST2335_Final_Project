package me.cuiziang.finalproject.ui;

import android.content.Context;
import android.content.Intent;
import android.support.v4.app.Fragment;

/**
 * The activity which host fragment of food list.
 */
public class FoodListActivity extends SingleFragmentActivity {

    public static Intent newIntent(Context packageContext) {
        return new Intent(packageContext, FoodListActivity.class);
    }

    @Override
    protected Fragment createFragment() {
        return FoodListFragment.newInstance();
    }
}
