package me.cuiziang.finalproject.ui;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AlertDialog;
import android.view.*;
import android.widget.*;
import jiulingzhang.movie_information.MovieMainActivity;
import me.cuiziang.finalproject.R;
import me.cuiziang.finalproject.db.entity.FoodEntity;
import me.cuiziang.finalproject.viewmodel.FoodViewModel;
import xiachen.ottransport.StationSearchActivity;
import yueli.cbcnewsreader.CBC_News_ReaderActivity;

import java.util.List;
import java.util.Objects;

/**
 * Controller of food list.
 */
public class FoodListFragment extends Fragment {

    private static final String TAG = "FOOD_LIST_FRAGMENT";

    private FoodAdapter foodAdapter;

    private FoodViewModel foodViewModel;

    private ListView foodListView;
    private Button foodSearchButton;
    private EditText keywordEditText;
    private TextView totalCaloriesTextView;
    private TextView averageCaloriesTextView;
    private TextView maxCaloriesTextView;
    private TextView minCaloriesTextView;

    private List<FoodEntity> foodList;

    public static FoodListFragment newInstance() {
        return new FoodListFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        View view = inflater.inflate(R.layout.fragment_food_list, container, false);

        foodListView = view.findViewById(R.id.foodListView);
        foodSearchButton = view.findViewById(R.id.foodSearchButton);
        keywordEditText = view.findViewById(R.id.keywordEditText);
        totalCaloriesTextView = view.findViewById(R.id.totalCaloriesTextView);
        averageCaloriesTextView = view.findViewById(R.id.averageCaloriesTextView);
        maxCaloriesTextView = view.findViewById(R.id.maxCaloriesTextView);
        minCaloriesTextView = view.findViewById(R.id.minCaloriesTextView);

        updateUI();

        foodSearchButton.setOnClickListener(v -> {
            FoodEntity food = new FoodEntity(keywordEditText.getText().toString());

            Intent intent = FoodDetailActivity.newIntent(getContext(), food);

            startActivity(intent);
            updateUI();
            keywordEditText.setText("");
        });

        foodListView.setOnItemClickListener((parent, view1, position, id) -> {

            FoodEntity food = foodAdapter.getItem(position);

            Intent intent = FoodDetailActivity.newIntent(getContext(), food);

            startActivity(intent);

        });

        foodListView.setOnItemLongClickListener((parent, view1, position, id) -> {
            LayoutInflater layoutInflater = this.getLayoutInflater();
            View tag_food_dialog = layoutInflater.inflate(R.layout.tag_food_dialog, null);

            FoodEntity food = foodAdapter.getItem(position);

            EditText tagFoodEditText = tag_food_dialog.findViewById(R.id.tagFoodEditText);
            tagFoodEditText.setText(food.getTag());

            AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
            builder.setView(tag_food_dialog).setPositiveButton("OK", (dialog1, which) -> {
                String tag = tagFoodEditText.getText().toString();
                food.setTag(tag);
                foodViewModel.updateFood(food);
                updateUI();
            }).setNegativeButton("Cancel", (dialog1, which) -> onStop()).show();
            return true;
        });
        return view;
    }

    private void updateUI() {
        foodViewModel = FoodViewModel.get(getContext());
        foodList = foodViewModel.getFoods();

        if (foodAdapter == null) {
            foodAdapter = new FoodAdapter(Objects.requireNonNull(this.getContext()), 0);
            foodListView.setAdapter(foodAdapter);
        } else {
            int totalCalories = foodViewModel.getTotalCalories();
            int averageCalories = foodViewModel.getAverageCalories();
            int maximumCalories = foodViewModel.getMaximumCalories();
            int minimumCalories = foodViewModel.getMinimumCalories();

            totalCaloriesTextView.setText(getString(R.string.totalCalories) + "\n" + totalCalories);
            averageCaloriesTextView.setText(getString(R.string.averageCalories) + "\n" + averageCalories);
            maxCaloriesTextView.setText(getString(R.string.maxCalories) + "\n" + maximumCalories);
            minCaloriesTextView.setText(getString(R.string.minCalories) + "\n" + minimumCalories);
            foodAdapter.setFoods(foodList);
            foodAdapter.notifyDataSetChanged();
        }
    }

    @Override

    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.fragment_food_nutrition, menu);
    }

    @Override
    public void onResume() {
        super.onResume();
        updateUI();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_item_help:
                FragmentManager manager = getFragmentManager();
                AboutFragment dialog = new AboutFragment();
                dialog.show(manager, "Test");
                return true;
            case R.id.to_movie:
                startActivity(new Intent(getContext(), MovieMainActivity.class));
                return true;
            case R.id.to_cbc:
                startActivity(new Intent(getContext(), CBC_News_ReaderActivity.class));
                return true;
            case R.id.to_transportation:
                startActivity(new Intent(getContext(), StationSearchActivity.class));
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private class FoodAdapter extends ArrayAdapter<FoodEntity> {
        public FoodAdapter(@android.support.annotation.NonNull Context context, int resource) {
            super(context, resource);
        }

        @Override
        public int getCount() {
            return foodList.size();
        }

        @android.support.annotation.Nullable
        @Override
        public FoodEntity getItem(int position) {
            return foodList.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @NonNull
        @Override
        public View getView(int position, View convertView, @android.support.annotation.NonNull ViewGroup parent) {
            LayoutInflater inflater = FoodListFragment.this.getLayoutInflater();

            @SuppressLint("ViewHolder") View result = inflater.inflate(R.layout.list_item_food, null);

            TextView foodNameTextView = result.findViewById(R.id.SearchedFoodNameTextView);

            foodNameTextView.setText(Objects.requireNonNull(getItem(position)).getTag());

            return result;
        }

        public void setFoods(List<FoodEntity> foods) {
            foodList = foods;
        }
    }
}
