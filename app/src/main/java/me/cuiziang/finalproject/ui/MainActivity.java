package me.cuiziang.finalproject.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import jiulingzhang.movie_information.MovieMainActivity;
import me.cuiziang.finalproject.R;
import xiachen.ottransport.StationSearchActivity;
import yueli.cbcnewsreader.CBC_News_ReaderActivity;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void onFoodInfoProjectClick(View view) {
        Intent intent = FoodListActivity.newIntent(MainActivity.this);
        startActivity(intent);
    }

    public void onMovieInfoProjectClick(View view) {
        Intent intent = new Intent(MainActivity.this, MovieMainActivity.class);
        startActivity(intent);
    }

    public void onOCTranspoProjectClick(View view) {
        Intent intent = new Intent(MainActivity.this, StationSearchActivity.class);
        startActivity(intent);
    }

    public void onCBCNewsReaderClick(View view) {
        Intent intent = new Intent(MainActivity.this, CBC_News_ReaderActivity.class);

        startActivity(intent);
    }
}
