package me.cuiziang.finalproject.viewmodel;

import android.arch.lifecycle.ViewModel;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import me.cuiziang.finalproject.db.FoodCursorWrapper;
import me.cuiziang.finalproject.db.FoodDatabaseHelper;
import me.cuiziang.finalproject.db.FoodDatabaseSchema;
import me.cuiziang.finalproject.db.entity.FoodEntity;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * View layer of the application
 */
public class FoodViewModel extends ViewModel {

    private static FoodViewModel foodViewModel;

    private Cursor cursor;

    private SQLiteDatabase sqliteDatabase;

    private FoodViewModel(Context context) {
        context = context.getApplicationContext();
        sqliteDatabase = new FoodDatabaseHelper(context).getWritableDatabase();
    }

    public static FoodViewModel get(Context context) {
        if (foodViewModel == null) {
            foodViewModel = new FoodViewModel(context);
        }
        return foodViewModel;
    }

    private static ContentValues getContentValues(FoodEntity food) {
        ContentValues values = new ContentValues();
        values.put(FoodDatabaseSchema.FoodTable.Cols.UUID, food.getId().toString());
        values.put(FoodDatabaseSchema.FoodTable.Cols.TAG, food.getTag());
        values.put(FoodDatabaseSchema.FoodTable.Cols.NAME, food.getName());
        values.put(FoodDatabaseSchema.FoodTable.Cols.CALORIE, food.getCalories());
        values.put(FoodDatabaseSchema.FoodTable.Cols.FAT, food.getFat());
        return values;
    }


    public void addFood(FoodEntity food) {
        ContentValues values = getContentValues(food);
        sqliteDatabase.insert(FoodDatabaseSchema.FoodTable.NAME, null, values);
    }

    private FoodCursorWrapper queryFoods(String whereClause, String[] whereArgs) {
        cursor = sqliteDatabase.query(FoodDatabaseSchema.FoodTable.NAME, null, whereClause, whereArgs, null, null, null);
        return new FoodCursorWrapper(cursor);
    }


    public List<FoodEntity> getFoods() {
        List<FoodEntity> foods = new ArrayList<>();
        try (FoodCursorWrapper cursor = queryFoods(null, null)) {
            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {
                foods.add(cursor.getFood());
                cursor.moveToNext();
            }
        }
        return foods;
    }


    public FoodEntity getFood(UUID id) {
        try (FoodCursorWrapper cursor = queryFoods(FoodDatabaseSchema.FoodTable.Cols.UUID + " = ?", new String[]{id.toString()})) {
            if (cursor.getCount() == 0) {
                return null;
            }
            cursor.moveToFirst();
            return cursor.getFood();
        }
    }

    public int getTotalCalories() {
        try {
            cursor = sqliteDatabase.rawQuery("SELECT SUM(" + FoodDatabaseSchema.FoodTable.Cols.CALORIE + ") FROM " + FoodDatabaseSchema.FoodTable.NAME, null);

            if (cursor.moveToFirst()) {
                return cursor.getInt(0);
            }
            return 0;
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
    }

    public int getAverageCalories() {
        try {
            cursor = sqliteDatabase.rawQuery("SELECT AVG(" + FoodDatabaseSchema.FoodTable.Cols.CALORIE + ") FROM " + FoodDatabaseSchema.FoodTable.NAME, null);
            if (cursor.moveToFirst()) {
                return cursor.getInt(0);
            }
            return 0;
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
    }

    public int getMaximumCalories() {
        try {
            cursor = sqliteDatabase.rawQuery("SELECT MAX(" + FoodDatabaseSchema.FoodTable.Cols.CALORIE + ") FROM " + FoodDatabaseSchema.FoodTable.NAME, null);
            if (cursor.moveToFirst()) {
                return cursor.getInt(0);
            }
            return 0;
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
    }

    public int getMinimumCalories() {
        try {
            cursor = sqliteDatabase.rawQuery("SELECT MIN(" + FoodDatabaseSchema.FoodTable.Cols.CALORIE + ") FROM " + FoodDatabaseSchema.FoodTable.NAME, null);
            if (cursor.moveToFirst()) {
                return cursor.getInt(0);
            }
            return 0;
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
    }

    public void updateFood(FoodEntity food) {
        String uuidString = food.getId().toString();
        ContentValues values = getContentValues(food);

        sqliteDatabase.update(FoodDatabaseSchema.FoodTable.NAME, values,
                FoodDatabaseSchema.FoodTable.Cols.UUID + " = ?", new String[]{uuidString});
    }

    public void deleteFood(UUID id) {

        sqliteDatabase.delete(FoodDatabaseSchema.FoodTable.NAME, FoodDatabaseSchema.FoodTable.Cols.UUID + " = ?", new String[]{id.toString()});
    }

}
