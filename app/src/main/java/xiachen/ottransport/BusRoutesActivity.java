package xiachen.ottransport;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserFactory;

import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import me.cuiziang.finalproject.R;

/**
 * Activity for bus routes from http://www.octranspo.com
 *
 * @author Xia Chen
 * @version 1.0
 */
public class BusRoutesActivity extends Activity {
    private ListView routeListView;
    private ArrayAdapter<String> listAdapter ;
    ArrayList<String> routeInfoList;
    ArrayList<String> routeNoList;
    /**
     * Override onCreate Method
     * initialize properties, set listeners, start querying from web server
     *
     * @param savedInstanceState Bundle
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bus_routes);

        Bundle infoToPass =  getIntent().getExtras();
        //repeat from tablet section:
        BusRoutesFragment newFragment = new BusRoutesFragment();
        newFragment.setIsTablet(false);
        newFragment.setArguments( infoToPass ); //give information to bundle

        FragmentManager fm = getFragmentManager();
        FragmentTransaction ftrans = fm.beginTransaction();
        ftrans.replace(R.id.empty_frame, newFragment); //load a fragment into the framelayout
        ftrans.addToBackStack("name doesn't matter"); //changes the back button behaviour
        ftrans.commit(); //actually load it
    }

}
