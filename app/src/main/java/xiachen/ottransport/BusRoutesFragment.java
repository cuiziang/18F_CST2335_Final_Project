package xiachen.ottransport;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Fragment;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserFactory;

import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

import me.cuiziang.finalproject.R;
/**
 * Bus routes fragment class
 *
 * @author Xia Chen
 * @version 1.0
 */
public class BusRoutesFragment extends Fragment {
    StationSearchActivity parent = null;
    private ListView routeListView;
    private ArrayAdapter<String> listAdapter ;
    ArrayList<String> routeInfoList;
    ArrayList<String> routeNoList;
    private boolean isTablet;
    View screen;

    /**
     * BusRoutesFragment
     * empty public constructor
     *
     * @param
     */
    public BusRoutesFragment() {
        // Required empty public constructor
    }

    /**
     * Override onCreate Method
     * initialize properties, set listeners, start querying from web server
     *
     * @param savedInstanceState Bundle
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

    }

    /**
     * Override onCreateView Method
     * setup route list view
     *
     * @param inflater LayoutInflater
     * @param container ViewGroup
      @param savedInstanceState Bundle
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Bundle infoToPass =  getArguments();
        final String stopNo = infoToPass.getString("StopNo");
        routeInfoList = infoToPass.getStringArrayList("RouteInfoList");
        routeNoList = infoToPass.getStringArrayList("RouteNoList");

        screen = inflater.inflate(R.layout.fragment_bus_routes, container, false);
        routeListView = (ListView)screen.findViewById(R.id.bus_routes_list);

        listAdapter = new ArrayAdapter<String>(screen.getContext(), R.layout.route_number, routeInfoList);
        routeListView.setAdapter( listAdapter );

        routeListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Log.i("Clicked", " position " + position + " id: " + id);
                new OCTTransportTripQuery().execute(stopNo, routeNoList.get(position));
            }
        });

        return screen;
    }

    /**
     * Override onAttach Method
     * assign parent context
     *
     * @param context Activity
     */
    @Override
    public void onAttach(Activity context) {
        super.onAttach(context);

        if(isTablet)
            parent = (StationSearchActivity)context; //find out which activity has the fragment
    }

    /**
     * setter for tablet
     *
     * @param isTablet boolean
     */
    public void setIsTablet(boolean isTablet) {
        this.isTablet = isTablet;
    }

    /**
     * Class for getting OC tranport trip information via asynctask
     *  @author Xia Chen
     *  @version 1.0
     */
    private class OCTTransportTripQuery extends AsyncTask<String, Integer, String> {
        String error_str;
        TripInfo[] tripInfoArray;
        String routeNo;
        final String NOT_AVAILABLE = "Not Available";

        /**
         * Override doInBackground Method
         * do http connect and parse xml data string
         *
         * @param args String...
         */
        @Override
        protected String doInBackground(String... args) {
            String url_summary = "https://api.octranspo1.com/v1.2/GetNextTripsForStop?appID=223eb5c3"+
                    "&&apiKey=ab27db5b435b8c8819ffb8095328e775&stopNo="+args[0]+"&routeNo="+args[1];
            routeNo = args[1];
            HttpURLConnection conn = null;
            try {
                URL url = new URL(url_summary);
                conn = (HttpURLConnection) url.openConnection();
                conn.setReadTimeout(10000);
                conn.setConnectTimeout(15000);
                conn.setRequestMethod("POST");
                conn.setRequestProperty("Content-Type", "text/xml");
                conn.setDoInput(true);
                conn.connect();
                int responseCode = conn.getResponseCode();
                tripInfoArray = new TripInfo[3];
                if (responseCode == HttpURLConnection.HTTP_OK) {
                    XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
                    factory.setNamespaceAware(true);
                    XmlPullParser xpp = factory.newPullParser();
                    xpp.setInput( conn.getInputStream(), null );
                    error_str = null;
                    int eventType = xpp.getEventType();
                    int count = 0;

                    while (eventType != XmlPullParser.END_DOCUMENT) {
                        if(eventType == XmlPullParser.START_DOCUMENT) {
                            System.out.println("Start document");
                        } else if(eventType == XmlPullParser.END_DOCUMENT) {
                            System.out.println("End document");
                        } else if(eventType == XmlPullParser.START_TAG) {
                            System.out.println("Start tag "+xpp.getName());
                            if (xpp.getName().equals("Error")) {
                                // step further to reach text for Error
                                if (xpp.next() == XmlPullParser.TEXT)
                                    error_str = xpp.getText();
                            }
                            else if (xpp.getName().equals("Trip")) {
                                TripInfo tripinfo = new TripInfo();
                                tripInfoArray[count] = tripinfo;
                            }
                            else if (xpp.getName().equals("TripDestination")) {
                                if (xpp.next() == XmlPullParser.TEXT)
                                    tripInfoArray[count].setTripDestination(xpp.getText());
                                else
                                    tripInfoArray[count].setTripDestination(NOT_AVAILABLE);
                            }
                            else if (xpp.getName().equals("TripStartTime")) {
                                if (xpp.next() == XmlPullParser.TEXT)
                                    tripInfoArray[count].setTripStartTime(xpp.getText());
                                else
                                    tripInfoArray[count].setTripStartTime(NOT_AVAILABLE);
                            }
                            else if (xpp.getName().equals("AdjustedScheduleTime")) {
                                if (xpp.next() == XmlPullParser.TEXT)
                                    tripInfoArray[count].setAdjustedScheduleTime(xpp.getText());
                                else
                                    tripInfoArray[count].setAdjustedScheduleTime(NOT_AVAILABLE);
                            }
                            else if (xpp.getName().equals("GPSSpeed")) {
                                if (xpp.next() == XmlPullParser.TEXT)
                                    tripInfoArray[count].setGpsSpeed(xpp.getText());
                                else
                                    tripInfoArray[count].setGpsSpeed(NOT_AVAILABLE);
                            }
                            else if (xpp.getName().equals("Latitude")) {
                                if (xpp.next() == XmlPullParser.TEXT)
                                    tripInfoArray[count].setLatitude(xpp.getText());
                                else
                                    tripInfoArray[count].setLatitude(NOT_AVAILABLE);
                            }
                            else if (xpp.getName().equals("Longitude")) {
                                if (xpp.next() == XmlPullParser.TEXT)
                                    tripInfoArray[count].setLongitude(xpp.getText());
                                else
                                    tripInfoArray[count].setLongitude(NOT_AVAILABLE);
                            }
                        } else if(eventType == XmlPullParser.END_TAG) {
                            System.out.println("End tag "+xpp.getName());
                            if (xpp.getName().equals("Trip")) {
                                count++;
                            }
                        } else if(eventType == XmlPullParser.TEXT) {
                            System.out.println("Text "+xpp.getText());
                        }
                        eventType = xpp.next();
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (conn != null) {
                    conn.disconnect();
                }
            }
            return null;
        }

        /**
         * Override onPostExecute Method
         * show bus route trip information or error message
         *
         * @param result String
         */
        @Override
        protected void onPostExecute(String result) {

            if (error_str != null) {
                String err_desc;
                if (error_str.equals("1"))
                    err_desc = "Invalid API key";
                else if (error_str.equals("2"))
                    err_desc = "Unable to query data source";
                else if (error_str.equals("10"))
                    err_desc = "Invalid stop number";
                else if (error_str.equals("11"))
                    err_desc = "Invalid route number";
                else if (error_str.equals("12"))
                    err_desc = "Stop does not service route";
                else
                    err_desc = "Unknown error";
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                        screen.getContext());

                alertDialogBuilder.setTitle("Error");
                alertDialogBuilder
                        .setMessage(err_desc)
                        .setCancelable(false)
                        .setPositiveButton("Ok", null);
                AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();
            }
            else {
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                        screen.getContext());

                alertDialogBuilder.setTitle("Trip information of route "+routeNo);
                StringBuilder trip_str = new StringBuilder();
                int index = 0;
                int validTripCount = 0;
                double totalScheduleTime = 0;
                for (TripInfo trip : tripInfoArray) {
                    if (trip != null) {
                        trip_str.append("Trip #").append(index+1).append("\n");
                        trip_str.append("TripDestination: ").append(trip.getTripDestination()).append("\n");
                        trip_str.append("TripStartTime: ").append(trip.getTripStartTime()).append("\n");
                        trip_str.append("AdjustedScheduleTime: ").append(trip.getAdjustedScheduleTime()).append("\n");
                        trip_str.append("GPSSpeed: ").append(trip.getGpsSpeed()).append("\n");
                        trip_str.append("Latitude: ").append(trip.getLatitude()).append("\n");
                        trip_str.append("Longitude: ").append(trip.getLongitude()).append("\n");
                        totalScheduleTime += Integer.parseInt(trip.getAdjustedScheduleTime());
                        validTripCount++;
                    }

                    index++;
                }
                if (validTripCount != 0)
                    totalScheduleTime /= validTripCount;
                trip_str.append("\nAverage Adjust Schedule Time: ").append(totalScheduleTime);
                alertDialogBuilder
                        .setMessage(trip_str)
                        .setCancelable(false)
                        .setPositiveButton("Ok", null);
                AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();
            }
        }
    }
}
