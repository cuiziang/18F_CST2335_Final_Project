package xiachen.ottransport;

import android.app.AlertDialog;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.*;
import android.widget.*;
import jiulingzhang.movie_information.MovieMainActivity;
import me.cuiziang.finalproject.R;
import me.cuiziang.finalproject.ui.FoodListActivity;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserFactory;
import yueli.cbcnewsreader.CBC_News_ReaderActivity;

import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

/**
 * Activity for searching Ottawa Transport information from http://www.octranspo.com
 *
 * @author Xia Chen
 * @version 1.0
 */
public class StationSearchActivity extends AppCompatActivity {
    /**
     * Search button
     */
    private Button searchButton;
    /**
     * Search edit text
     */
    private EditText searchText;
    /**
     * Station information list view
     */
    private ListView stationInfoListView;
    /**
     * Search record
     */
    private ArrayList<SearchData> searchRecord;
    /**
     * Station listView adaptor
     */
    private BusStationAdapter stnAdapter;
    /**
     * Search progress bar
     */
    private ProgressBar progressBar;

    private ArrayList<StopInfo> stopInfoList;

    private boolean isTablet = false;
    BusRoutesFragment busRouteFragment;
    SQLiteDatabase db;
    Cursor cursor;
    /**
     * Override onCreate Method
     * initialize properties, set listeners, start querying from web server
     *
     * @param savedInstanceState Bundle
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_station_search);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        if (findViewById(R.id.fragment_location) != null) {
            isTablet = true;
        }

        searchRecord = new ArrayList<>();
        StationSearchDbHelper dbOpener = new StationSearchDbHelper(this);
        db = dbOpener.getWritableDatabase();
        cursor = db.query(StationSearchDbHelper.TABLE_NAME,
                new String[]{StationSearchDbHelper.COLUMN_ID, StationSearchDbHelper.COLUMN_STATION_NO},
                null, null, null,null, null, null );
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            String stationNo = cursor.getString(cursor.getColumnIndex( StationSearchDbHelper.COLUMN_STATION_NO));
            long dbID = cursor.getLong(cursor.getColumnIndex(StationSearchDbHelper.COLUMN_ID));
            SearchData data = new SearchData(stationNo, dbID);
            searchRecord.add(data);
            cursor.moveToNext();
        }
        cursor.close();

        searchText = (EditText) findViewById(R.id.search_input);
        searchButton = (Button) findViewById(R.id.search_button);
        stationInfoListView = (ListView) findViewById(R.id.StationInfoList);
        stopInfoList = new ArrayList<>();
        stnAdapter = new BusStationAdapter( this );
        stationInfoListView.setAdapter(stnAdapter);
        progressBar = findViewById(R.id.progressBar);
        progressBar.setVisibility(View.INVISIBLE);

        searchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (searchText.getText().toString().isEmpty()) {
                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                            StationSearchActivity.this);
                    alertDialogBuilder.setTitle("Error");
                    alertDialogBuilder
                            .setMessage("Please input station number!")
                            .setCancelable(false)
                            .setPositiveButton("Ok", null);
                    AlertDialog alertDialog = alertDialogBuilder.create();
                    alertDialog.show();
                    return;
                }

                Toast.makeText(getApplicationContext(), "Searching...", Toast.LENGTH_SHORT).show();
                //ListView listView = (ListView) findViewById(R.id.StationInfoList);
                LinearLayout linearLayout = (LinearLayout) findViewById(R.id.station_serach_linearLayout);
                Snackbar.make(linearLayout, "Snackbar: "+searchText.getText().toString(), Snackbar.LENGTH_LONG).show();
                progressBar.setVisibility(View.VISIBLE);
                //progressBar.setProgress(100);

                new OCTTransportQuery().execute(searchText.getText().toString(), "add_record");
            }
        });

        stationInfoListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Log.i("Clicked", " position " + position + " id: " + id);

                if (!searchRecord.get(position).isUpdated()) {
                    new OCTTransportQuery().execute(searchRecord.get(position).getStationNo(), "");
                    return;
                }
                //go to but route activity:
                Bundle infoToPass = new Bundle();
                StopInfo stopRoutes = stopInfoList.get(position);
                infoToPass.putString("StopNo", stopRoutes.getStopNo());
                infoToPass.putStringArrayList("RouteNoList", stopRoutes.getRouteNoList());
                infoToPass.putStringArrayList("RouteInfoList", stopRoutes.getRouteInfoList());

                if (isTablet) {
                    busRouteFragment = new BusRoutesFragment();

                    busRouteFragment.setArguments(infoToPass); //give information to bundle
                    busRouteFragment.setIsTablet(true);
                    FragmentManager fm = getFragmentManager();
                    FragmentTransaction fTrans = fm.beginTransaction();
                    fTrans.replace(R.id.fragment_location, busRouteFragment); //load a fragment into the framelayout
                    fTrans.commit(); //actually load it
                }
                else {
                    Intent nextPage = new Intent(StationSearchActivity.this, BusRoutesActivity.class);
                    nextPage.putExtras(infoToPass);
                    startActivityForResult(nextPage, 50);
                }
            }
        });

        stationInfoListView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long rowId) {
                // Store selected item in global variable
                String selectedItem = stnAdapter.getItem(position).toString();

                AlertDialog.Builder builder = new AlertDialog.Builder(StationSearchActivity.this);
                builder.setMessage("Do you want to remove " + selectedItem + "?");
                builder.setCancelable(false);
                builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        db.delete(StationSearchDbHelper.TABLE_NAME, "_id=?", new String[]{Long.toString(stnAdapter.getItemId(position))});
                        searchRecord.remove(position);
                        stnAdapter.notifyDataSetChanged();
                        Toast.makeText(
                                getApplicationContext(),
                                selectedItem + " has been removed.",
                                Toast.LENGTH_SHORT).show();
                    }
                });
                builder.setNegativeButton("No", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });

                // Create and show the dialog
                builder.show();

                // Signal OK to avoid further processing of the long click
                return true;
            }
        });
    }

    /**
     * Override onCreateOptionsMenu Method
     * create option menus
     *
     * @param menu Menu
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.ottransport_menu, menu);
        return true;
    }

    /**
     * Override onOptionsItemSelected Method
     * create menu items
     *
     * @param item MenuItem
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        Intent intent;
        switch (id){
            case R.id.ottransport_to_food:
                intent = new Intent(StationSearchActivity.this, FoodListActivity.class);
                startActivity(intent);
                return true;
            case R.id.ottransport_to_cbc:
                intent = new Intent(StationSearchActivity.this, CBC_News_ReaderActivity.class);
                startActivity(intent);
                return true;
            case R.id.ottransport_to_movie:
                intent = new Intent(StationSearchActivity.this, MovieMainActivity.class);
                startActivity(intent);
                return true;
            case R.id.ottransport_help:
                String title = getResources().getString(R.string.station_serach_instruction_title);
                String instruction = getResources().getString(R.string.station_serach_instruction);
                String ok = getResources().getString(R.string.station_serach_ok);
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                        StationSearchActivity.this);

                alertDialogBuilder.setTitle(title);
                alertDialogBuilder
                        .setMessage(instruction)
                        .setCancelable(false)
                        .setPositiveButton(ok, null);
                AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
    /**
     * Class for search data. Station number and database ID
     * @author Xia Chen
     * @version 1.0
     */
    private class SearchData {
        String stationNo;
        long dbID;
        boolean updated;

        public SearchData() {
            stationNo = "";
            dbID = 0;
            updated = false;
        }
        public SearchData(String stationNo, long dbID) {
            this.stationNo = stationNo;
            this.dbID = dbID;
            updated = false;
        }
        public String getStationNo() {
            return stationNo;
        }
        public void setStationNo(String stationNo) {
            this.stationNo = stationNo;
        }
        public long getDbID() {
            return dbID;
        }
        public void setDbID(long dbID) {
            this.dbID = dbID;
        }
        public void setUpdated() {this.updated = true;}
        public boolean isUpdated() { return updated; }
    }

    /**
     * Class for adapting local arraylist to listview
     * @author Xia Chen
     * @version 1.0
     */
    private class BusStationAdapter extends ArrayAdapter<String> {
        public BusStationAdapter(Context ctx) { super(ctx, 0); }

        @Override
        public String getItem(int position) { return searchRecord.get(position).getStationNo(); }

        @Override
        public long getItemId(int position){ return searchRecord.get(position).getDbID(); }

        public int getCount(){ return searchRecord.size(); }

        public View getView(int position, View oldView, ViewGroup parent ) {
            LayoutInflater inflater = StationSearchActivity.this.getLayoutInflater();
            View result = inflater.inflate(R.layout.station_number, null) ;
            TextView stationNo = (TextView)result.findViewById(R.id.station_number);
            stationNo.setText( getItem(position) );
            return result;
        }
    }

    /**
     * Class for getting OC tranport information via asynctask
     * @author Xia Chen
     * @version 1.0
     */
    private class OCTTransportQuery extends AsyncTask<String, Integer, String> {
        String error_str;
        StopInfo stopRoutes;
        final String NOT_AVAILABLE = "Not Available";
        String stationNoForSearch;
        boolean add_record;
        /**
         * Override doInBackground Method
         * connect http server to get xml data string for routeNo list
         *
         * @param args String
         */
        @Override
        protected String doInBackground(String... args) {
            stationNoForSearch = args[0];
            add_record = args[1].equals("add_record");
            String url_summary = "https://api.octranspo1.com/v1.2/GetRouteSummaryForStop?appID=223eb5c3&&apiKey=ab27db5b435b8c8819ffb8095328e775&stopNo="+stationNoForSearch;
            HttpURLConnection conn = null;
            stopRoutes = new StopInfo(stationNoForSearch);
            publishProgress(0);
            try {
                URL url = new URL(url_summary);
                conn = (HttpURLConnection) url.openConnection();
                conn.setReadTimeout(10000);
                conn.setConnectTimeout(15000);
                conn.setRequestMethod("POST");
                conn.setRequestProperty("Content-Type", "text/xml");
                conn.setDoInput(true);
                conn.connect();
                publishProgress(20);
                int responseCode = conn.getResponseCode();
                if (responseCode == HttpURLConnection.HTTP_OK) {
                    XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
                    factory.setNamespaceAware(true);
                    XmlPullParser xpp = factory.newPullParser();
                    xpp.setInput( conn.getInputStream(), null );
                    error_str = null;
                    int eventType = xpp.getEventType();
                    int count = 0;
                    while (eventType != XmlPullParser.END_DOCUMENT) {
                        if(eventType == XmlPullParser.START_DOCUMENT) {
                            System.out.println("Start document");
                        } else if(eventType == XmlPullParser.END_DOCUMENT) {
                            System.out.println("End document");
                        } else if(eventType == XmlPullParser.START_TAG) {
                            System.out.println("Start tag "+xpp.getName());
                            if (xpp.getName().equals("Error")) {
                                // step further to reach text for Error
                                if (xpp.next() == XmlPullParser.TEXT)
                                    error_str = xpp.getText();
                            }
                            else if (xpp.getName().equals("Route")) {
                                stopRoutes.createRouteInfo();
                            }
                            else if (xpp.getName().equals("RouteNo")) {
                                // step further to reach text for RouteNo
                                if (xpp.next() == XmlPullParser.TEXT)
                                    stopRoutes.setRouteNo(xpp.getText(), count);
                                else
                                    stopRoutes.setRouteNo(NOT_AVAILABLE, count);
                            }
                            else if (xpp.getName().equals("Direction")) {
                                // step further to reach text for Direction
                                if (xpp.next() == XmlPullParser.TEXT)
                                    stopRoutes.setDirection(xpp.getText(), count);
                                else
                                    stopRoutes.setDirection(NOT_AVAILABLE, count);
                            }
                            else if (xpp.getName().equals("RouteHeading")) {
                                // step further to reach text for RouteHeading
                                if (xpp.next() == XmlPullParser.TEXT)
                                    stopRoutes.setRouteHeading(xpp.getText(), count);
                                else
                                    stopRoutes.setRouteHeading(NOT_AVAILABLE, count);
                            }
                        } else if(eventType == XmlPullParser.END_TAG) {
                            System.out.println("End tag "+xpp.getName());
                            if (xpp.getName().equals("Route")) {
                                count++;
                                publishProgress(count%100);
                            }
                        } else if(eventType == XmlPullParser.TEXT) {
                            System.out.println("Text "+xpp.getText());
                        }
                        eventType = xpp.next();
                    }
                    publishProgress(100);
                    if (error_str == null) {
                        stopInfoList.add(stopRoutes);
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (conn != null) {
                    conn.disconnect();
                }
            }
            return null;
        }
        /**
         * Override onProgressUpdate Method
         * increase progress value
         *
         * @param value Integer
         */
        @Override
        protected void onProgressUpdate(Integer... value) {
            progressBar.setVisibility(View.VISIBLE);
            progressBar.setProgress(value[0]);
        }
        /**
         * Override onPostExecute Method
         * show routeNo list for stopNo
         *
         * @param result String
         */
        @Override
        protected void onPostExecute(String result) {
            progressBar.setVisibility(View.INVISIBLE);
            if (error_str != null) {
                searchText.setText("");
                String err_desc;
                if (error_str.equals("1"))
                    err_desc = "Invalid API key";
                else if (error_str.equals("2"))
                    err_desc = "Unable to query data source";
                else if (error_str.equals("10"))
                    err_desc = "Invalid stop number";
                else if (error_str.equals("11"))
                    err_desc = "Invalid route number";
                else if (error_str.equals("12"))
                    err_desc = "Stop does not service route";
                else
                    err_desc = "Unknown error";
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                        StationSearchActivity.this);

                alertDialogBuilder.setTitle("Error");
                alertDialogBuilder
                        .setMessage(err_desc)
                        .setCancelable(false)
                        .setPositiveButton("Ok", null);
                AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();
            }
            else {
                if (add_record) {
                    ContentValues newRow = new ContentValues();
                    newRow.put(StationSearchDbHelper.COLUMN_STATION_NO, searchText.getText().toString());
                    long dbID = db.insert(StationSearchDbHelper.TABLE_NAME, "ReplacementValue", newRow);
                    SearchData data = new SearchData(searchText.getText().toString(), dbID);
                    data.setUpdated();
                    searchRecord.add(data);
                }
                stnAdapter.notifyDataSetChanged();
                searchText.setText("");

                // prepare info to pass
                Bundle infoToPass = new Bundle();
                infoToPass.putString("StopNo", stopRoutes.getStopNo());
                infoToPass.putStringArrayList("RouteNoList", stopRoutes.getRouteNoList());
                infoToPass.putStringArrayList("RouteInfoList", stopRoutes.getRouteInfoList());

                if (isTablet) {
                    busRouteFragment = new BusRoutesFragment();

                    busRouteFragment.setArguments(infoToPass); //give information to bundle
                    busRouteFragment.setIsTablet(true);
                    FragmentManager fm = getFragmentManager();
                    FragmentTransaction fTrans = fm.beginTransaction();
                    fTrans.replace(R.id.fragment_location, busRouteFragment); //load a fragment into the framelayout
                    fTrans.commit(); //actually load it
                }
                else {
                    Intent nextPage = new Intent(StationSearchActivity.this, BusRoutesActivity.class);
                    nextPage.putExtras(infoToPass);
                    startActivity(nextPage);
                }
            }
        }
    }

    /**
     * Class for database helper
     * @author Xia Chen
     * @version 1.0
     */
    class StationSearchDbHelper extends SQLiteOpenHelper {
        public static final String DATABASE_NAME = "StationSearchDB";
        public static final int VERSION_NUM = 3;
        public static final String TABLE_NAME = "MSG_TABLE";
        public static final String COLUMN_ID = "_id";
        public static final String COLUMN_STATION_NO = "StationNo";

        public StationSearchDbHelper(Context ctx) {
            super(ctx, DATABASE_NAME, null, VERSION_NUM);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            Log.i("ChatDatabaseHelper", "Calling onCreate");
            db.execSQL("CREATE TABLE " + TABLE_NAME + " ("+ COLUMN_ID+" INTEGER PRIMARY KEY AUTOINCREMENT," + COLUMN_STATION_NO + " text);");
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            Log.i("ChatDatabaseHelper", "Calling onUpgrade, oldVersion=" + oldVersion +  "newVersion=" + newVersion);
            db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
            onCreate(db);
        }
    }
}
