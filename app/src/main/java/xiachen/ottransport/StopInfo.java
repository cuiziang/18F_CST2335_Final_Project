package xiachen.ottransport;

import java.util.ArrayList;

/**
 * Class for stop information with routeNo list
 * @author Xia Chen
 * @version 1.0
 */
public class StopInfo {
    /**
     * stop number. For example 3017. Each stopNo has multiple routeInfo
     */
    String stopNo;
    /**
     * routes number list. Each routeInfo has up to 3 tripInfo
     */
    ArrayList<RouteInfo> routeInfoList;

    /**
     * constructor
     * initialize stopNo and routeInfoList
     *
     * @param stopNo String
     */
    public StopInfo(String stopNo){
        this.stopNo = stopNo;
        routeInfoList = new ArrayList<>();
    }
    /**
     * setter for StopNo
     *
     * @param stopNo String
     */
    public void setStopNo( String stopNo){ this.stopNo = stopNo; }
    /**
     * getter for StopNo
     *
     * @param
     */
    public String getStopNo(){ return stopNo; }
    /**
     * create route info and add it to routeInfolist
     *
     */
    public void createRouteInfo() {
        RouteInfo routeInfo = new RouteInfo();
        routeInfoList.add(routeInfo);
    }
    /**
     * setter for RouteNo with array index
     *
     * @param routeNo String
     * @param index int
     */
    public void setRouteNo(String routeNo, int index) {
        routeInfoList.get(index).setRouteNo(routeNo);
    }
    /**
     * setter for Direction with array index
     *
     * @param direction String
     * @param index int
     */
    public void setDirection(String direction, int index) {
        routeInfoList.get(index).setDirection(direction);
    }
    /**
     * setter for routeHeading with array index
     *
     * @param routeHeading String
     * @param index int
     */
    public void setRouteHeading(String routeHeading, int index) {
        routeInfoList.get(index).setRouteHeading(routeHeading);
    }
    /**
     * get routeInfo with index
     *
     * @param index int
     */
    public RouteInfo getRouteInfo(int index){ return routeInfoList.get(index);}
    /**
     * get routeInfoList array reference
     *
     */
    public ArrayList<String> getRouteInfoList() {
        ArrayList<String> routeString = new ArrayList<>();
        for (RouteInfo route: routeInfoList) {
            routeString.add(route.toString());
        }
        return routeString;
    }
    /**
     * get routeNo array reference
     *
     */
    public ArrayList<String> getRouteNoList() {
        ArrayList<String> routeNoList = new ArrayList<>();
        for (RouteInfo route: routeInfoList) {
            routeNoList.add(route.getRouteNo());
        }
        return routeNoList;
    }

    /**
     * Class for route trip details
     * @author Xia Chen
     * @version 1.0
     */
    class RouteInfo {
        String routeNo;
        String direction;
        String routeHeading;

        public void setDirection(String direction) { this.direction = direction; }
        public String getDirection() { return direction; }
        public void setRouteNo(String routeNo) { this.routeNo = routeNo; }
        public String getRouteHeading() { return routeHeading; }
        public void setRouteHeading(String routeHeading) { this.routeHeading = routeHeading; }
        public String getRouteNo() { return routeNo; }

        /**
         * Override toString Method
         * create trip information in a single string
         *
         * @param
         */
        @Override
        public String toString() {
            return (routeNo+"\nDirection: "+direction+"\nHeading to: "+routeHeading);
        }
    }

}
