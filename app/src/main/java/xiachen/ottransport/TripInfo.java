package xiachen.ottransport;

/**
 * Class for trip information for each routeNo
 * @author Xia Chen
 * @version 1.0
 */
public class TripInfo {
    String tripDestination;
    String latitude;
    String longitude;
    String gpsSpeed;
    String tripStartTime;
    String adjustedScheduleTime;

    /**
     * getter for TripDestination
     *
     */
    public String getTripDestination() {
        return tripDestination;
    }
    /**
     * getter for Latitude
     *
     */
    public String getLatitude() {
        return latitude;
    }
    /**
     * getter for Longitude
     *
     */
    public String getLongitude() {
        return longitude;
    }
    /**
     * getter for GpsSpeed
     *
     */
    public String getGpsSpeed() {
        return gpsSpeed;
    }
    /**
     * getter for TripStartTime
     *
     */
    public String getTripStartTime() {
        return tripStartTime;
    }
    /**
     * getter for AdjustedScheduleTime
     *
     */
    public String getAdjustedScheduleTime() {
        return adjustedScheduleTime;
    }
    /**
     * setter for TripDestination
     * @param tripDestination String
     */
    public void setTripDestination(String tripDestination) {
        this.tripDestination = tripDestination;
    }
    /**
     * setter for AdjustedScheduleTime
     * @param adjustedScheduleTime String
     */
    public void setAdjustedScheduleTime(String adjustedScheduleTime) {
        this.adjustedScheduleTime = adjustedScheduleTime;
    }
    /**
     * setter for GpsSpeed
     * @param gpsSpeed String
     */
    public void setGpsSpeed(String gpsSpeed) {
        this.gpsSpeed = gpsSpeed;
    }
    /**
     * setter for Latitude
     * @param latitude String
     */
    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }
    /**
     * setter for Longitude
     * @param longitude String
     */
    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }
    /**
     * setter for TripStartTime
     * @param tripStartTime String
     */
    public void setTripStartTime(String tripStartTime) {
        this.tripStartTime = tripStartTime;
    }
}
