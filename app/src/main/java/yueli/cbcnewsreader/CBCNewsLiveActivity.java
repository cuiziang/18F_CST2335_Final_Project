package yueli.cbcnewsreader;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.util.Xml;
import android.view.*;
import android.widget.*;
import jiulingzhang.movie_information.MovieMainActivity;
import me.cuiziang.finalproject.R;
import me.cuiziang.finalproject.foodlist.FoodListActivity;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import xiachen.ottransport.StationSearchActivity;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;


/**
 * author: Yueli Zhu
 * version: 0.1
 * description: CBC News Reader Activity
 */
public class CBCNewsLiveActivity extends AppCompatActivity {
    EditText ed1;
    ProgressBar progressBar;
    ListView listview;
    ArrayList<News> newslist = new ArrayList<News>();
    CBCNewsReaderAdapter messageAdapter = null;
    private static final String TAG = "CBCNewsLiveActivity";

    /**
     * Override the onCreate() to create the activity, including initilize properties and
     * start a seperate web access task to retrive information
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cbcnews_live);
        Toolbar toolbar = (Toolbar)findViewById(R.id.cbc_news_reader_toolbar);
        setSupportActionBar(toolbar);

        listview=(ListView)findViewById(R.id.newsListView);
        progressBar = (ProgressBar)findViewById(R.id.progressBar);
        progressBar.setVisibility(View.VISIBLE);
        messageAdapter = new CBCNewsReaderAdapter(this);
        listview.setAdapter(messageAdapter);
        ed1=(EditText)findViewById(R.id.edit1);

        listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                News news = newslist.get(position);
                Log.i(TAG, "item clicked: position: " + position + " id: " + id);

                //go to new window:
                Intent intent = new Intent(CBCNewsLiveActivity.this, NewsDetailsActivity.class);
                intent.putExtra("title", news.title);
                intent.putExtra("pubDate", news.pubDate);
                intent.putExtra("category", news.category);
                intent.putExtra("author", news.author);
                intent.putExtra("description", news.description);
                intent.putExtra("link", news.link);
                intent.putExtra("position",position);
                intent.putExtra("id",id);
                intent.putExtra("parentIsLiveSearch",true);

                startActivityForResult(intent, 10);
            }
        });

        new QueryCBCTask().execute("https://www.cbc.ca/cmlink/rss-world");
    }


    public void onDestroy(){
        //db.close();
        super.onDestroy();
        Log.i(TAG,"in onDestroy()");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.cbc_news_menu,menu);

        return true;
        //return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()){
            //case R.id.cbc_news_menu_item1:
            //{
            //    Toast.makeText(this," item is selected", Toast.LENGTH_SHORT).show();
            //}
            //   return true;
            case R.id.cbc_news_menu_item2:
            {
                AlertDialog.Builder builder = new AlertDialog.Builder(CBCNewsLiveActivity.this);
                builder.setTitle(R.string.cbc_news_help_title)
                        .setMessage(R.string.cbc_news_help_message)
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                            }
                        });
                AlertDialog dialog = builder.create();
                dialog.show();
            }
            return true;

            case R.id.ic_food:
                startActivity(new Intent(CBCNewsLiveActivity.this, FoodListActivity.class));
                return true;
            case R.id.ic_movie:
                startActivity(new Intent(CBCNewsLiveActivity.this, MovieMainActivity.class));
                return true;
            case R.id.ic_transportation:
                startActivity(new Intent(CBCNewsLiveActivity.this, StationSearchActivity.class));
                return true;
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    public void deleteMessage(int position, long id){
        //dbHelper.delete(id, db);
        //newslist.remove(position);

        //messageAdapter.notifyDataSetChanged();
    }

    public void onActivityResult(int requestCode, int responseCode, Intent data){
        if(requestCode == 10  && responseCode == 10) {
            Bundle bundle = data.getExtras();
            int position = bundle.getInt("position");
            long id = bundle.getLong("id");
            deleteMessage(position, id);

        }
    }

    /**
     * A welcome dialog pops up when the button is clicked
     * @param view
     */
    public void onButtonClick(View view) {

        Toast.makeText(getApplicationContext(), "This is a toast", Toast.LENGTH_SHORT).show();
        Snackbar.make((Button) findViewById(R.id.button), "This a Snackbar", Snackbar.LENGTH_LONG).show();
        ed1.setText("");
    }

    /**
     * An adapter class to adapt to the list view
     */
    private class CBCNewsReaderAdapter extends ArrayAdapter<News> {
        public CBCNewsReaderAdapter(Context ctx){
            super(ctx,0);
        }


        @Override
        public long getItemId(int pos){
            return pos;
        }

        /**
         * returns the number of items
         * @return
         */
        @Override
        public int getCount(){
            //TODO
            return newslist.size();
        }

        /**
         * returns an item from a specific positionto
         * @param pos
         * @return
         */
        @Override
        public News getItem(int pos){
            //ToDo
            return newslist.get(pos);
        }

        /**
         * return a story title to be shown at row position
         * @param position
         * @param convertView
         * @param parent
         * @return
         */
        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater = CBCNewsLiveActivity.this.getLayoutInflater();
            View rowView = inflater.inflate(R.layout.news_item_row, null);
            TextView textView = (TextView) rowView.findViewById(R.id.textView);
            textView.setText(getItem(position).title);

            return rowView;
        }
    }

    /**
     * Class to open a URL and return a stream
     * @param urlString: URL address
     * @return
     * @throws IOException
     */
    private InputStream OpenHttpConnection(String urlString) throws IOException
    {
        InputStream in = null;
        int response = -1;

        URL url = new URL(urlString);
        URLConnection conn = url.openConnection();

        if (!(conn instanceof HttpURLConnection))
            throw new IOException("Not an HTTP connection");
        try{
            HttpURLConnection httpConn = (HttpURLConnection) conn;
            httpConn.setReadTimeout(10000 /* milliseconds */);
            httpConn.setConnectTimeout(15000 /* milliseconds */);
            httpConn.setRequestMethod("GET");
            httpConn.setDoInput(true);
            httpConn.connect();
            response = httpConn.getResponseCode();
            if (response == HttpURLConnection.HTTP_OK) {
                in = httpConn.getInputStream();
            }
        }
        catch (Exception ex)
        {
            Log.i(TAG, ex.getLocalizedMessage());
            throw new IOException("Error connecting");
        }
        return in;
    }


    /**
     * A seperate Task to query CBC web site
     */
    private class QueryCBCTask extends AsyncTask<String, Integer, String> {
        InputStream in = null;

        /**
         * Override doInBackground class
         * @param urls
         * @return
         */
        @Override
        protected String doInBackground(String... urls) {
            String iconName = "";
            String title = null;
            String link = null;
            URL url = null;

            try {
                in = OpenHttpConnection(urls[0]);
                //in.close();
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }

            try {
                XmlPullParser parser = Xml.newPullParser();
                parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
                parser.setInput(in, null);
                parser.nextTag();
                //return readFeed(parser);

                //parser.require(XmlPullParser.START_TAG, null, "current");
                while (parser.next() != XmlPullParser.END_DOCUMENT) {
                    if (parser.getEventType() != XmlPullParser.START_TAG) {
                        continue;
                    }
                    String name = parser.getName();
                    // Starts by looking for the entry tag
                    if (name.equals("item")) {
                        newslist.add(readItem(parser));
                    }
                }
                in.close();
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }

            return null;
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);
            //progressBar.setProgress(values[0]);
        }

        /**
         * Override onPostExecute class to handle when web query task is finished
         * @param str
         */
        @Override
        protected void onPostExecute(String str) {
            //super.onPostExecute(result);
            messageAdapter.notifyDataSetChanged();
            progressBar.setVisibility(View.GONE);
        }
    }

    /** Parses the contents of an item. If it encounters a title, or link tag, hands them off
     *  to their respective "read" methods for processing. Otherwise, skips the tag.
     */
    private News readItem(XmlPullParser parser) throws XmlPullParserException, IOException {
        parser.require(XmlPullParser.START_TAG, null, "item");
        String title = null;
        String link = null;
        String guid = null;
        String pubDate = null;
        String author = null;
        String category = null;
        String description = null;
        while (parser.next() != XmlPullParser.END_TAG) {
            if (parser.getEventType() != XmlPullParser.START_TAG) {
                continue;
            }
            String name = parser.getName();
            if (name.equals("title")) {
                title = readTitle(parser);
            } else if (name.equals("link")) {
                link = readLink(parser);
            } else if (name.equals("guid")) {
                guid = readGuid(parser);
            } else if (name.equals("pubDate")) {
                pubDate = readPubDate(parser);
            } else if (name.equals("author")) {
                author = readAuthor(parser);
            } else if (name.equals("category")) {
                category = readCategory(parser);
            } else if (name.equals("description")) {
                description = readDescription(parser);
            }
        }
        return new News(title, link, guid, pubDate, author, category, description);
    }


    /**
     * Retrive the title of the news
     * @param parser
     * @return
     * @throws IOException
     * @throws XmlPullParserException
     */
    private String readTitle(XmlPullParser parser) throws IOException, XmlPullParserException {
        parser.require(XmlPullParser.START_TAG, null, "title");
        String string = readText(parser);
        parser.require(XmlPullParser.END_TAG, null, "title");
        return string;
    }

    /**
     * Retrive the link of the news
     * @param parser
     * @return
     * @throws IOException
     * @throws XmlPullParserException
     */
    private String readLink(XmlPullParser parser) throws IOException, XmlPullParserException {
        parser.require(XmlPullParser.START_TAG, null, "link");
        String string = readText(parser);
        parser.require(XmlPullParser.END_TAG, null, "link");
        return string;
    }

    private String readGuid(XmlPullParser parser) throws IOException, XmlPullParserException {
        parser.require(XmlPullParser.START_TAG, null, "guid");
        String string = readText(parser);
        parser.require(XmlPullParser.END_TAG, null, "guid");
        return string;
    }

    private String readPubDate(XmlPullParser parser) throws IOException, XmlPullParserException {
        parser.require(XmlPullParser.START_TAG, null, "pubDate");
        String string = readText(parser);
        parser.require(XmlPullParser.END_TAG, null, "pubDate");
        return string;
    }

    private String readAuthor(XmlPullParser parser) throws IOException, XmlPullParserException {
        parser.require(XmlPullParser.START_TAG, null, "author");
        String string = readText(parser);
        parser.require(XmlPullParser.END_TAG, null, "author");
        return string;
    }

    private String readCategory(XmlPullParser parser) throws IOException, XmlPullParserException {
        parser.require(XmlPullParser.START_TAG, null, "category");
        String string = readText(parser);
        parser.require(XmlPullParser.END_TAG, null, "category");
        return string;
    }

    private String readDescription(XmlPullParser parser) throws IOException, XmlPullParserException {
        parser.require(XmlPullParser.START_TAG, null, "description");
        String string = readText(parser);
        parser.require(XmlPullParser.END_TAG, null, "description");
        return string;
    }

    // For the tags title and summary, extracts their text values.
    private String readText(XmlPullParser parser) throws IOException, XmlPullParserException {
        String result = "";
        if (parser.next() == XmlPullParser.TEXT) {
            result = parser.getText();
            parser.nextTag();
        }
        return result;
    }
}
