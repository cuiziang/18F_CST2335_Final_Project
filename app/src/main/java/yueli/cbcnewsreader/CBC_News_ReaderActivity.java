package yueli.cbcnewsreader;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.util.Xml;
import android.view.*;
import android.widget.*;
import jiulingzhang.movie_information.MovieMainActivity;
import me.cuiziang.finalproject.R;
import me.cuiziang.finalproject.ui.FoodListActivity;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import xiachen.ottransport.StationSearchActivity;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;

/**
 * author: Yueli Zhu
 * version: 0.1
 * description: CBC News Reader Activity
 */
public class CBC_News_ReaderActivity extends AppCompatActivity {
    private static final String TAG = "CBC_News_ReaderActivity";

    /**
     * Override the onCreate() to create the activity, including initilize properties and
     * start a seperate web access task to retrive information
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cbc__news__reader);
        Toolbar toolbar = (Toolbar)findViewById(R.id.cbc_news_reader_toolbar);
        setSupportActionBar(toolbar);
    }

    public void onDestroy(){
        super.onDestroy();
        Log.i(TAG,"in onDestroy()");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.cbc_news_menu,menu);

        return true;
        //return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()){
            //case R.id.cbc_news_menu_item1:
            //{
            //    Toast.makeText(this," item is selected", Toast.LENGTH_SHORT).show();
            //}
            //   return true;
            case R.id.cbc_news_menu_item2:
                {
                    AlertDialog.Builder builder = new AlertDialog.Builder(CBC_News_ReaderActivity.this);
                    builder.setTitle(R.string.cbc_news_help_title)
                            .setMessage(R.string.cbc_news_help_message)
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                            }
                    });
                    AlertDialog dialog = builder.create();
                    dialog.show();
                }
                return true;

            case R.id.ic_food:
                startActivity(new Intent(CBC_News_ReaderActivity.this, FoodListActivity.class));
                return true;
            case R.id.ic_movie:
                startActivity(new Intent(CBC_News_ReaderActivity.this, MovieMainActivity.class));
                return true;
            case R.id.ic_transportation:
                startActivity(new Intent(CBC_News_ReaderActivity.this, StationSearchActivity.class));
                return true;
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    public void onActivityResult(int requestCode, int responseCode, Intent data){
        if(requestCode == 10  && responseCode == 10) {
            Log.i(TAG,"onActivityResult, "+"responseCode: " + responseCode);
        }
    }

    /**
     * A welcome dialog pops up when the button is clicked
     * @param view
     */
    public void onButtonClick(View view) {
/*

        Toast.makeText(getApplicationContext(), "This is a toast", Toast.LENGTH_SHORT).show();
        Snackbar.make((Button) findViewById(R.id.button), "This a Snackbar", Snackbar.LENGTH_LONG).show();
*/
        startActivity(new Intent(CBC_News_ReaderActivity.this, CBCNewsLiveActivity.class));
    }

    public void onButton2Click(View view) {
        startActivity(new Intent(CBC_News_ReaderActivity.this, NewsFavoriteActivity.class));
    }
}

