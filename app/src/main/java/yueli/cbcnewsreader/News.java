package yueli.cbcnewsreader;

/**
 * A class News to structure the information for each story
 */
public class News {
    public final String title;
    public final String link;
    //public final String guid;
    public final String pubDate;
    public final String author;
    public final String category;
    public final String description;

    News(String title, String link, String guid, String pubDate, String author, String category, String description) {
        this.title = title;
        this.link = link;
        //this.guid = guid;
        this.pubDate = pubDate;
        this.author = author;
        this.category = category;
        this.description = description;
    }
}