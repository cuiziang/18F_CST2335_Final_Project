package yueli.cbcnewsreader;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class NewsDatabaseHelper  extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "news_favorite.db";
    private static int VERSION_NUM = 1;
    static final String TABLENAME = " NewsFavorite";
    static final String KEY_ID = "_id";
    static final String KEY_TITLE = "TITLE";
    static final String KEY_LINK = "LINK";
    static final String KEY_PUBDATE = "PUBDATE";
    static final String KEY_AUTHOR = "AUTHOR";
    static final String KEY_CATEGORY = "CATEGORY";
    static final String KEY_DESCRIPTION = "DESCRIPTION";
/*    private static final String DATABASE_CREATE = "create table " + TABLENAME + "( " + KEY_ID + " integer primary key autoincrement, " + KEY_TITLE + " text not null);";*/
/*
    private static final String DATABASE_CREATE = "create table " + TABLENAME + "( " + KEY_ID + " integer primary key autoincrement, " + KEY_TITLE + " text not null, " + KEY_LINK + " text not null);";*/
/*
    private static final String DATABASE_CREATE = "create table " + TABLENAME + "( " + KEY_ID + " integer primary key autoincrement, " + KEY_TITLE + " text not null, " + KEY_LINK + " text not null, " + KEY_PUBDATE + " text not null, " + KEY_AUTHOR + " text not null, " + KEY_CATEGORY + " text not null, " + KEY_DESCRIPTION + " text not null);";
*/

/*
    private static final String DATABASE_CREATE = "CREATE TABLE " + TABLENAME + "" +
            "(" + KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " + KEY_TITLE + " TEXT NOT NULL, "
            + KEY_LINK + " TEXT NOT NULL, "  + KEY_PUBDATE + " TEXT NOT NULL, "+ KEY_DESCRIPTION + " TEXT NOT NULL"+ ")";
*/
/*
    private static final String DATABASE_CREATE = "CREATE TABLE " + TABLENAME + "" +
            "(" + KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT ," + KEY_TITLE + " TEXT NOT NULL,"
            + KEY_LINK + " TEXT NOT NULL"+ ")";*/



    private static final String TAG = "NewsDatabaseHelper";

    NewsDatabaseHelper(Context ctx){
        super(ctx,DATABASE_NAME,null,VERSION_NUM);
    }

    @Override
    public void onCreate(SQLiteDatabase db){
        //db.execSQL(DATABASE_CREATE);

        db.execSQL(
                "create table " + TABLENAME
                        + "(" + KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                        + KEY_TITLE + " TEXT NOT NULL, "
                        + KEY_LINK + " TEXT NOT NULL, "
                        + KEY_PUBDATE + " TEXT NOT NULL, "
                        + KEY_DESCRIPTION + " TEXT NOT NULL)"
        );
        Log.i(TAG, "Calling onCreate");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        /*VERSION_NUM++*/;
        Log.i(TAG, "Calling onUpgrade, oldVersion=" + oldVersion + " newVersion=" + newVersion);
        db.execSQL("DROP TABLE IF EXISTS " + TABLENAME);
        onCreate(db);
    }

    public static int getIdFromCursor(Cursor cursor){
        int id = cursor.getInt(cursor.getColumnIndex(KEY_ID));
        return id;
    }

    public static boolean delete(long id, SQLiteDatabase db){
        int tmp = 0;

        Log.i(TAG, "Calling delete");
        tmp = db.delete(TABLENAME, KEY_ID + "=" + id, null);
        return (tmp > 0);
    }
}
