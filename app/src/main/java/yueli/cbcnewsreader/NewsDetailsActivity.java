package yueli.cbcnewsreader;

import android.app.Activity;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.util.Log;
import me.cuiziang.finalproject.R;

import java.util.Objects;


/**
 * author: Yueli Zhu
 * version: 0.1
 * description: News Details Activity
 */
public class NewsDetailsActivity extends Activity {
    private static final String TAG = "NewsDetailsActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news_details);

        Log.i(TAG, "in onCreate()");
        String title = Objects.requireNonNull(getIntent().getExtras()).getString("title");
        String pubDate = getIntent().getExtras().getString("pubDate");
        String category = getIntent().getExtras().getString("category");
        String author = getIntent().getExtras().getString("author");
        String description = getIntent().getExtras().getString("description");
        String link = getIntent().getExtras().getString("link");
        int position = getIntent().getExtras().getInt("position");
        long id = getIntent().getExtras().getLong("id");
        boolean forLiveNews = getIntent().getExtras().getBoolean("parentIsLiveSearch",true);

        NewsDetailsFragment newFragment = new NewsDetailsFragment();
        newFragment.forLiveNews = forLiveNews;
        Bundle bundle = new Bundle();
        bundle.putString("title", title);
        bundle.putString("pubDate", pubDate);
        bundle.putString("category", category);
        bundle.putString("author", author);
        bundle.putString("description", description);
        bundle.putString("link", link);
        bundle.putInt("position",position);
        bundle.putLong("id",id);

        newFragment.setArguments(bundle);


        FragmentManager fm = getFragmentManager();
        FragmentTransaction ftrans = fm.beginTransaction();
        ftrans.replace(R.id.news_detail, newFragment);
        ftrans.addToBackStack(null);
        ftrans.commit();

    }
}
