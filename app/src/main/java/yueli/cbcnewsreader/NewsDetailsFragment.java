package yueli.cbcnewsreader;


import android.app.Activity;
import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.os.Bundle;
//import android.support.v4.app.Fragment;
import android.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import android.widget.Toast;
import me.cuiziang.finalproject.R;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;


/**
 * author: Yueli Zhu
 * version: 0.1
 * description: News Details Fragment
 */
public class NewsDetailsFragment extends Fragment {

    TextView tv_title;
    TextView tv_pubDate;
    TextView tv_author;
    TextView tv_description;
    TextView tv_link;
    Button Btn, Btn2, Btn3;
    String title;
    String pubDate;
    String category;
    String author;
    String description;
    String link;
    int position;
    long id;
    public boolean forLiveNews;
    NewsDatabaseHelper dbHelper;
    SQLiteDatabase db;

    private static final String TAG = "NewsDetailsFragment";

    public NewsDetailsFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Log.i(TAG, "onCreateView()");

        // Inflate the layout for this fragment
        View screen = inflater.inflate(R.layout.fragment_news_details, container, false);

        Bundle infoToPass = getArguments(); //returns the arguments set before
        title = infoToPass.getString("title");
        pubDate = infoToPass.getString("pubDate");
        category = infoToPass.getString("category");
        author = infoToPass.getString("author");
        description = infoToPass.getString("description");
        link = infoToPass.getString("link");
        position = infoToPass.getInt("position");
        id = infoToPass.getLong("id");

        dbHelper = new NewsDatabaseHelper(getActivity());
        Log.i(TAG, "db.open,Helper.getWritableDatabase");
        db = dbHelper.getWritableDatabase();

        tv_title = (TextView) screen.findViewById(R.id.title);
        tv_title.setText(getResources().getString(R.string.cbc_news_details_title) + " :" + title);

        tv_pubDate = (TextView) screen.findViewById(R.id.pubDate);
        tv_pubDate.setText(getResources().getString(R.string.cbc_news_details_pubdate) + " :" + pubDate);

        tv_author = (TextView) screen.findViewById(R.id.author);
        tv_author.setText(getResources().getString(R.string.cbc_news_details_author) + " :" + author);

        tv_description = (TextView) screen.findViewById(R.id.description);
        tv_description.setText(getResources().getString(R.string.cbc_news_details_description) + " :" + description);

        tv_link = (TextView) screen.findViewById(R.id.link);
        //tv_link.setText("link is: " + link);
        tv_link.setText(link);

        Btn = (Button) screen.findViewById(R.id.cbc_news_details_button);

        if (forLiveNews == false) {
            //Btn.setText("delete");
            Btn.setVisibility(View.INVISIBLE);
        }

        Btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (forLiveNews) {
                    Log.i(TAG, "news details save button is clicked");
/*                Intent intent = new Intent();
                intent.putExtra("title", title);
                intent.putExtra("position", position);
                intent.putExtra("id", id);
                getActivity().setResult(10, intent);
                getActivity().finish();// go to previous activity*/

/*
                if (!fileExistance(title+".txt")) {
                    Log.i(TAG, title+".txt"+" is not found locally, downloading it now");
                    try{
                        FileOutputStream outputStream = getActivity().openFileOutput(title+".txt", Context.MODE_PRIVATE);
                        outputStream.write(description.getBytes());
                        outputStream.flush();
                        outputStream.close();
                    }catch(Exception e){
                        e.printStackTrace();
                    }

                    int word_count = description.length();

                    Statistic stat = Statistic.getInstance();
                    stat.num_acticles ++;
                    stat.total_word_count += word_count;

                    if((stat.min_word_count ==0)||(word_count < stat.min_word_count))
                        stat.min_word_count = word_count;
                    if(word_count > stat.max_word_count)
                        stat.max_word_count = word_count;

                    Toast.makeText(getActivity(), title+".txt"+" is saved now", Toast.LENGTH_LONG).show();
                }
                else{
                    Log.i(TAG, title+".txt"+" is found locally");
                    Toast.makeText(getActivity(), title+".txt"+" is already saved", Toast.LENGTH_LONG).show();
                }*/


                    boolean alreadysaved = false;
                    Cursor c = db.rawQuery("select * from " + NewsDatabaseHelper.TABLENAME, new String[]{});

                    int colIndexTitle = c.getColumnIndex(NewsDatabaseHelper.KEY_TITLE);
                    int colIndexLink = c.getColumnIndex(NewsDatabaseHelper.KEY_LINK);
                    int colIndexPubdate = c.getColumnIndex(NewsDatabaseHelper.KEY_PUBDATE);
                    int colIndexDescription = c.getColumnIndex(NewsDatabaseHelper.KEY_DESCRIPTION);
                    Log.i(TAG, "Cursor's column count=" + c.getColumnCount());
                    c.moveToFirst();
                    while (!c.isAfterLast()) {
                        News news;
                        long id = c.getLong(c.getColumnIndex(NewsDatabaseHelper.KEY_ID));
                        String t_title = c.getString(colIndexTitle);
                        if (t_title.equals(title)) {
                            alreadysaved = true;
                            break;
                        }

                        Log.i(TAG, "SQL title:" + title);
                        Log.i(TAG, "SQL ID: " + id);

                        c.moveToNext();
                    }
                    if (alreadysaved == false) {
                        ContentValues args = new ContentValues();
                        args.put(NewsDatabaseHelper.KEY_TITLE, title);
                        args.put(NewsDatabaseHelper.KEY_LINK, link);
                        args.put(NewsDatabaseHelper.KEY_PUBDATE, pubDate);
/*                    args.put(NewsDatabaseHelper.KEY_AUTHOR, author);
                    args.put(NewsDatabaseHelper.KEY_CATEGORY, category);*/
                        args.put(NewsDatabaseHelper.KEY_DESCRIPTION, description);
                        long id = db.insert(NewsDatabaseHelper.TABLENAME, null, args);
                        Log.i(TAG, "id: " + id);


                        int word_count = description.length();

                        Statistic stat = Statistic.getInstance();
                        stat.num_acticles++;
                        stat.total_word_count += word_count;

                        if ((stat.min_word_count == 0) || (word_count < stat.min_word_count))
                            stat.min_word_count = word_count;
                        if (word_count > stat.max_word_count)
                            stat.max_word_count = word_count;

                        Toast.makeText(getActivity(), title + ".txt" + " is saved now", Toast.LENGTH_LONG).show();

                    } else {
                        Toast.makeText(getActivity(), "this story is already saved", Toast.LENGTH_SHORT).show();
                    }

                } else {
                    Log.i(TAG, "news details delete button is clicked");
                }
                db.close();
                Log.i(TAG,"db.close()");
            }
        });

        Btn2 = (Button) screen.findViewById(R.id.cbc_news_details_button2);
        if (forLiveNews == false)
            Btn2.setVisibility(View.INVISIBLE);

        Btn2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.i(TAG, "news details statistics button is clicked");
                Statistic stat = Statistic.getInstance();

                //Toast.makeText(getActivity(), "Saved files in total is " + stat.num_acticles, Toast.LENGTH_LONG).show();


/*                {
                    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                    builder.setTitle(R.string.cbc_news_details_stat_title)
                            .setMessage(R.string.cbc_news_details_stat_num_article + "\n")
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                }
                            });
                    AlertDialog dialog = builder.create();
                    dialog.show();
                }*/

                {
                    int average_word_account = 0;
                    String str1 = null;
                    if (stat.num_acticles != 0) {
                        average_word_account = stat.total_word_count / stat.num_acticles;
                        str1 = Integer.toString(average_word_account);
                    } else {
                        str1 = "";
                    }

                    String str = getResources().getString(R.string.cbc_news_details_stat_num_article) + Integer.toString(stat.num_acticles) + "\n"
                            + getResources().getString(R.string.cbc_news_details_stat_max_word_count) + Integer.toString(stat.max_word_count) + "\n"
                            + getResources().getString(R.string.cbc_news_details_stat_min_word_count) + Integer.toString(stat.min_word_count) + "\n"
                            + getResources().getString(R.string.cbc_news_details_stat_average_word_count) + str1 + "\n";
                    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                    builder.setTitle(R.string.cbc_news_details_stat_title)
                            .setMessage(str)
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                }
                            });
                    AlertDialog dialog = builder.create();
                    dialog.show();
                }

            }
        });


        Btn3 = (Button) screen.findViewById(R.id.cbc_news_details_button3);

        Btn3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.i(TAG, "news details button is clicked");
                Intent intent = new Intent();
                //intent.putExtra("title", title);
                intent.putExtra("position", position);
                intent.putExtra("id", id);
                getActivity().setResult(10, intent);
                getActivity().finish();// go to previous activity
            }
        });

        return screen;
    }

    @Override
    public void onAttach(Activity context) {
        super.onAttach(context);
        Log.i(TAG, "onAttach()");

    }

    @Override
    public void onDetach() {
        super.onDetach();
        Log.i(TAG, "onDetach()");
    }

    @Override
    public void onDestroyView() {
        //db.close();
        super.onDestroyView();
        Log.i(TAG, "onDestroyView()");
    }


    /**
     * @param fname file name
     * @return if the file exists, true. Otherwise false.
     */
    public boolean fileExistance(String fname) {
        File file = getActivity().getFileStreamPath(fname);
        return file.exists();
    }

}


/**
 * A Singleton class for managing statistic info
 */
class Statistic {
    private static Statistic single_instance = null;

    public int num_acticles;
    public int min_word_count;
    public int max_word_count;
    public int total_word_count;

    private Statistic() {
        num_acticles = 0;
        min_word_count = 0;
        max_word_count = 0;
        total_word_count = 0;
    }

    public static Statistic getInstance() {
        if (single_instance == null) {
            single_instance = new Statistic();
        }
        return single_instance;
    }
}
