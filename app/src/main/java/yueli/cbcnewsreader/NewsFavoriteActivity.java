package yueli.cbcnewsreader;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import me.cuiziang.finalproject.R;

import java.util.ArrayList;

public class NewsFavoriteActivity extends AppCompatActivity {

    ListView listview;
    ArrayList<News> newslist = new ArrayList<News>();
    NewsDatabaseHelper dbHelper;
    SQLiteDatabase db;
    private NewsFavoriteAdapter messageAdapter;
    private static final String TAG = "NewsFavoriteActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news_favorite);

        listview=(ListView)findViewById(R.id.NewsFavoriteView);
        messageAdapter = new NewsFavoriteAdapter(this);
        listview.setAdapter(messageAdapter);

        dbHelper = new NewsDatabaseHelper(this);
        db = dbHelper.getWritableDatabase();
        Cursor c = db.rawQuery("select * from " + NewsDatabaseHelper.TABLENAME , new String[]{});

        int colIndexTitle = c.getColumnIndex(NewsDatabaseHelper.KEY_TITLE);
        int colIndexLink = c.getColumnIndex(NewsDatabaseHelper.KEY_LINK);
        int colIndexPubdate = c.getColumnIndex(NewsDatabaseHelper.KEY_PUBDATE);
        int colIndexDescription = c.getColumnIndex(NewsDatabaseHelper.KEY_DESCRIPTION);
        Log.i(TAG,"Cursor's column count=" + c.getColumnCount());
        c.moveToFirst();
        while(!c.isAfterLast() ){
            News news;
            long id = c.getLong(c.getColumnIndex(NewsDatabaseHelper.KEY_ID));
            String title = c.getString( colIndexTitle );
            String link = c.getString( colIndexLink );
            String pubDate = c.getString( colIndexPubdate );
            String description = c.getString( colIndexDescription );

            //news = new News(title, link, guid, pubDate, author, category, description);
            news = new News(title, link, "guid", pubDate, "author", "category", description);
            newslist.add(news);

            Log.i(TAG,"SQL title:" + title);
            Log.i(TAG,"SQL ID: " + id);

            c.moveToNext();
        }
        messageAdapter.notifyDataSetChanged();

        listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                News news = newslist.get(position);
                Log.i(TAG, "item clicked: position: " + position + " id: " + id);

                //go to new window:
                Intent intent = new Intent(NewsFavoriteActivity.this, NewsDetailsActivity.class);
                intent.putExtra("title", news.title);
                intent.putExtra("pubDate", news.pubDate);
                intent.putExtra("category", news.category);
                intent.putExtra("author", news.author);
                intent.putExtra("description", news.description);
                intent.putExtra("link", news.link);
                intent.putExtra("position",position);
                intent.putExtra("id",id);
                intent.putExtra("parentIsLiveSearch",false);

                startActivityForResult(intent, 10);
            }
        });
    }

    public void onDestroy(){
        db.close();
        super.onDestroy();
        Log.i(TAG,"in onDestroy()");
    }

    private class NewsFavoriteAdapter extends ArrayAdapter<News> {
        public NewsFavoriteAdapter(Context context) {
            super(context, 0);
        }

        @Override
        public long getItemId(int position) {
            Cursor cursor = db.rawQuery("select * from " + NewsDatabaseHelper.TABLENAME, new String[]{});
            cursor.moveToPosition(position);
            int  id = NewsDatabaseHelper.getIdFromCursor(cursor);
            return id;
        }

        @Override
        public int getCount() {
            return newslist.size();
        }

        @Override
        public News getItem(int position) {
            return newslist.get(position);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater = NewsFavoriteActivity.this.getLayoutInflater();
            View rowView = inflater.inflate(R.layout.news_item_row, null);
            TextView textView = (TextView) rowView.findViewById(R.id.textView);
            textView.setText(getItem(position).title);

            return rowView;
        }
    }
}

